package com.davidminaya.metas.models

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import com.davidminaya.metas.receivers.TareasReceiver
import com.davidminaya.metas.database.MetasDB
import com.davidminaya.metas.database.entities.Horario
import com.davidminaya.metas.utils.*
import java.lang.ref.WeakReference

class HorarioActivityViewModel(app: Application): AndroidViewModel(app) {

    private val db = MetasDB.getMetasDB(app)
    private lateinit var horarioEliminadoListener: () -> Unit

    fun horarioTareasDias(horarioId: Int) = db.horarioDao().horarioTareasDias(horarioId)

    fun eliminarHorario(horario: Horario) {
        EliminarHorarioAsynkTask(this).execute(horario)
    }

    fun horarioEliminadoListener(listener: () -> Unit) {
        horarioEliminadoListener = listener
    }

    private class EliminarHorarioAsynkTask(model: HorarioActivityViewModel): AsyncTask<Horario, Unit, Unit>() {

        private val referencia = WeakReference(model)

        override fun doInBackground(vararg params: Horario) {

            val model = referencia.get() ?: return
            val horario = params[0]

            eliminarAlarmas(model, horario)

            model.db.horarioDao().delete(horario)
            model.db.diasDao().borrarDiasDelHorario(horario.horarioId!!)
        }

        private fun eliminarAlarmas(model: HorarioActivityViewModel, horario: Horario) {

            val context = model.getApplication<Application>()
            val tarea = model.db.tareaDao().ultimaTarea(horario.horarioId!!)
            val dias = model.db.diasDao().dias(horario.horarioId!!)

            if (tarea.isFinalizada) return

            for (dia in dias) {

                val intent = Intent(context, TareasReceiver::class.java).apply { action = ACTION_EJECUTAR_TAREA }
                val pendingId = (tarea.tareaId!! * 7) + dia
                val pendingIntent = PendingIntent.getBroadcast(model.getApplication(), pendingId, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager.cancel(pendingIntent)
            }
        }

        override fun onPostExecute(result: Unit?) {
            val model = referencia.get() ?: return
            model.horarioEliminadoListener()
        }
    }
}