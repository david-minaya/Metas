package com.davidminaya.metas.models

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.arch.lifecycle.*
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import com.davidminaya.metas.R
import com.davidminaya.metas.receivers.TareasReceiver
import com.davidminaya.metas.database.MetasDB
import com.davidminaya.metas.database.entities.Horario
import com.davidminaya.metas.database.entities.Tarea
import com.davidminaya.metas.utils.*
import com.davidminaya.metas.utils.ParcelableUtil
import org.threeten.bp.*
import org.threeten.bp.format.TextStyle
import org.threeten.bp.temporal.ChronoField
import org.threeten.bp.temporal.WeekFields
import java.lang.ref.WeakReference
import java.util.*

/**
 * Creada por david minaya el 21/09/2018 10:13.
 *
 */
class TareaViewModel(private val appContext: Application) : AndroidViewModel(appContext) {

    companion object {
        const val TAREA_CREADA = 0
        const val TAREA_NO_CREADA = 1
    }

    private val db: MetasDB = MetasDB.getMetasDB(appContext)
    val horarios: LiveData<MutableList<Horario>>
    val proximaEjecucion: MutableLiveData<String>
    private var handler: Handler
    private lateinit var onCrearTareaListener: (Int) -> Unit

    init {

        val thread = HandlerThread("Agregar tarea view model thread")
        thread.start()
        handler = Handler(thread.looper)

        horarios = db.horarioDao().horarios()
        proximaEjecucion = MutableLiveData()
    }

    fun setOnCrearTareaListener(onCrearTareaListener: (Int) -> Unit) {
        this.onCrearTareaListener = onCrearTareaListener
    }

    fun saveTarea(tarea: Tarea) {
        SaveTareaAsyncTask(db, appContext, onCrearTareaListener).execute(tarea)
    }

    fun actualizarTarea(tarea: Tarea) {

        handler.post{
            db.tareaDao().update(tarea)
        }
    }

    fun eliminarTarea(tarea: Tarea) {
        EliminarTareaAsyncTask(db).execute(tarea)
    }

    fun proximaEjecucion(tarea: Tarea, owner: LifecycleOwner, listener: Observer<String>) {
        proximaEjecucion.observe(owner, listener)
        ConsultarProximaEjecucion(this).execute(tarea)
    }

    private class SaveTareaAsyncTask(val db: MetasDB, val appContext: Application, val listener: (Int) -> Unit): AsyncTask<Tarea, Void, Int>() {

        override fun doInBackground(vararg params: Tarea?): Int {

            val tarea = params[0]!!
            val existeAlgunaTareaEnEjecucion = db.tareaDao().existeAlgunaTareaEnEjecucion(tarea.horarioId!!).isNotEmpty()

            if (!existeAlgunaTareaEnEjecucion) {

                tarea.tareaId = db.tareaDao().insertarTarea(tarea).toInt()
                crearAlarmas(tarea)

                return TAREA_CREADA
            }

            return TAREA_NO_CREADA
        }

        override fun onPostExecute(result: Int) {
            listener(result)
        }

        /**
         * Crea las alarmas en las que se ejecutara la tarea.
         *
         * Por cada dia en que se ejecuta la tarea se crea una alarma.
         * Por ejemplo si una tarea se ejecuta los dias lunes, martes
         * y miercoles, se crean tres alarmas.
         * */
        private fun crearAlarmas(tarea: Tarea) {

            val horarioDias = db.horarioDao().horarioDias(tarea.horarioId!!)

            val horaDeInicio = LocalTime.parse(horarioDias.horaDeInicio)
            val diaActual = LocalDate.now().dayOfWeek.value
            val horaActual = LocalTime.now()

            for (dia in horarioDias.dias!!) {

                val nDia = dia.nombreDiaId!!.toLong()
                var fechaDeEjecucion: Long

                if ((nDia >= diaActual && horaDeInicio > horaActual) || nDia > diaActual) {

                    fechaDeEjecucion = LocalDateTime.now()
                            .with(ChronoField.DAY_OF_WEEK, nDia)
                            .withHour(horaDeInicio.hour)
                            .withMinute(horaDeInicio.minute)
                            .withSecond(0)
                            .toInstant(OffsetDateTime.now().offset) // Obtiene la zona horaria actual
                            .toEpochMilli()

                } else {

                    fechaDeEjecucion = LocalDateTime.now()
                            .plusWeeks(1)
                            .with(ChronoField.DAY_OF_WEEK, nDia)
                            .withHour(horaDeInicio.hour)
                            .withMinute(horaDeInicio.minute)
                            .withSecond(0)
                            .toInstant(OffsetDateTime.now().offset) // Obtiene la zona horaria actual
                            .toEpochMilli()
                }

                val intent = Intent(appContext, TareasReceiver::class.java).apply {
                    action = ACTION_EJECUTAR_TAREA
                    putExtra(EXTRA_TAREA, ParcelableUtil.marshall(tarea))
                }

                val pendingId = ((tarea.tareaId!! * 7) + nDia).toInt()
                val pendingIntent = PendingIntent.getBroadcast(appContext, pendingId, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                val alarmManager = appContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, fechaDeEjecucion, pendingIntent)
                } else {
                    alarmManager.setExact(AlarmManager.RTC, fechaDeEjecucion, pendingIntent)
                }
            }
        }
    }

    private class EliminarTareaAsyncTask(val db: MetasDB): AsyncTask<Tarea, Unit, Unit>() {
        override fun doInBackground(vararg params: Tarea?) {
            db.tareaDao().delete(params[0]!!)
        }
    }

    private class ConsultarProximaEjecucion(model: TareaViewModel): AsyncTask<Tarea?, Unit, Unit>() {

        val referencia = WeakReference(model)

        override fun doInBackground(vararg params: Tarea?) {

            val model = referencia.get() ?: return
            val tarea = params[0]!!
            val horarioDias = model.db.horarioDao().horarioDias(tarea.horarioId!!)

            var fechaDeEjecucionProximaTarea: LocalDateTime? = null
            val horaDeInicio = horarioDias.horaDeInicio!!.toTime()
            val fechaActual = LocalDateTime.now()
            val diaActual = fechaActual.dayOfWeek.value
            val horaActual = fechaActual.toLocalTime()
            val dias = horarioDias.dias!!

            for (dia in dias) {

                val nDia = dia.nombreDiaId!!.toLong()
                var fechaDeEjecucion: LocalDateTime

                if ((nDia >= diaActual && horaDeInicio > horaActual) || nDia > diaActual) {

                    fechaDeEjecucion = LocalDateTime.now()
                            .with(ChronoField.DAY_OF_WEEK, nDia)
                            .withHour(horaDeInicio.hour)
                            .withMinute(horaDeInicio.minute)
                            .withSecond(0)

                } else {

                    fechaDeEjecucion = LocalDateTime.now()
                            .plusWeeks(1)
                            .with(ChronoField.DAY_OF_WEEK, nDia)
                            .withHour(horaDeInicio.hour)
                            .withMinute(horaDeInicio.minute)
                            .withSecond(0)
                }

                if (fechaDeEjecucionProximaTarea == null) {
                    fechaDeEjecucionProximaTarea = fechaDeEjecucion
                }

                if (fechaDeEjecucion < fechaDeEjecucionProximaTarea) {
                    fechaDeEjecucionProximaTarea = fechaDeEjecucion
                }
            }

            if (fechaDeEjecucionProximaTarea == null) return

            val semana = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear()

            val esHoy = fechaActual.dayOfMonth == fechaDeEjecucionProximaTarea.dayOfMonth
            val esManana = fechaActual.plusDays(1).dayOfMonth == fechaDeEjecucionProximaTarea.dayOfMonth
            val esEnEstaSemana = fechaActual.get(semana) == fechaDeEjecucionProximaTarea.get(semana) && !esHoy && !esManana

            var nombreDia = fechaDeEjecucionProximaTarea.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.forLanguageTag("es"))
            nombreDia = nombreDia.substring(0, 1).capitalize() + nombreDia.substring(1, nombreDia.length)

            val hoy = model.appContext.getString(R.string.hoy)
            val manana = model.appContext.getString(R.string.manana)
            val este = model.appContext.getString(R.string.este)
            val proximo = model.appContext.getString(R.string.proximo)

            val proximaEjecucionString = when {
                esHoy -> hoy
                esManana -> manana
                esEnEstaSemana -> "$este $nombreDia"
                else -> "$proximo $nombreDia"
            }

            model.proximaEjecucion.postValue(proximaEjecucionString)
        }
    }
}