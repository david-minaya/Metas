package com.davidminaya.metas.models

import android.app.Application
import android.arch.lifecycle.AndroidViewModel

import com.davidminaya.metas.database.MetasDB

/**
 * Creada por david minaya el 09/08/2018 6:09.
 */
class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val metasDB: MetasDB = MetasDB.getMetasDB(application)

    val getHorariosWithTareas
        get() = metasDB.horarioDao().horariosTareasDias

    val getHorariosTareasDias
        get() = metasDB.horarioDao().horariosTareaDias()

}
