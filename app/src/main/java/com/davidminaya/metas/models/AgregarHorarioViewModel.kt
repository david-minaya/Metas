package com.davidminaya.metas.models

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.util.Log
import com.davidminaya.metas.R
import com.davidminaya.metas.receivers.TareasReceiver
import com.davidminaya.metas.database.MetasDB
import com.davidminaya.metas.database.entities.Dia
import com.davidminaya.metas.database.entities.Horario
import com.davidminaya.metas.database.entities.NombreDia
import com.davidminaya.metas.utils.ACTION_EJECUTAR_TAREA
import com.davidminaya.metas.utils.EXTRA_TAREA
import com.davidminaya.metas.utils.ParcelableUtil
import com.davidminaya.metas.utils.toTime
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.temporal.ChronoField
import java.lang.Exception

/**
 * Creada por david minaya el 02/10/2018 1:15.
 */
class AgregarHorarioViewModel(val app: Application): AndroidViewModel(app) {

    companion object {

        const val HORARIO_CREADO = 0
        const val HORARIO_ACTUALIZADO = 1
        const val ERROR_HORARIO_EXISTENTE = 2
        const val ERROR_HORA_DE_INICIO = 3
    }

    val db: MetasDB = MetasDB.getMetasDB(app)
    val dias = mutableListOf<NombreDia>()
    private lateinit var agregarHorarioListener: (Int) -> Unit

    init {
        dias.add(NombreDia(app.resources.getString(R.string.lunes), 1))
        dias.add(NombreDia(app.resources.getString(R.string.martes), 2))
        dias.add(NombreDia(app.resources.getString(R.string.miercoles), 3))
        dias.add(NombreDia(app.resources.getString(R.string.jueves), 4))
        dias.add(NombreDia(app.resources.getString(R.string.viernes), 5))
        dias.add(NombreDia(app.resources.getString(R.string.sabado), 6))
        dias.add(NombreDia(app.resources.getString(R.string.domingo), 7))
    }

    fun insertHorario(horario: Horario, dias: MutableList<NombreDia>) {
        InsertHorarioAsync().execute(horario, dias)
    }

    fun actualizaHorario(horario: Horario, diasSeleccionados: List<NombreDia>) {
        ActualizarHorarioAsync().execute(horario, diasSeleccionados)
    }

    fun setAgregarHorarioListenerListener(agregarHorarioListener: (Int) -> Unit) {
        this.agregarHorarioListener = agregarHorarioListener
    }

    private inner class InsertHorarioAsync: AsyncTask<Any, Unit, Int>() {

        override fun doInBackground(vararg params: Any): Int {

            val nuevoHorario = params[0] as Horario
            val dias = params[1] as MutableList<*>

            val crearHorario: Boolean

            try {
                crearHorario = colicionaConOtrosHorarios(nuevoHorario, dias)
            } catch (e: Exception) {
                return ERROR_HORA_DE_INICIO
            }

            if (crearHorario) {

                val horarioId = db.horarioDao().insert(nuevoHorario).toInt()

                Log.i("horario", "horarioId: $horarioId")

                val dias2 = mutableListOf<Dia>()
                for (dia in dias) {
                    dia as NombreDia
                    dias2.add(Dia(horarioId, dia.nombreDiaId!!))
                }

                db.diasDao().insert(*dias2.toTypedArray())

                return HORARIO_CREADO
            }

            return ERROR_HORARIO_EXISTENTE
        }

        override fun onPostExecute(result: Int) {

            agregarHorarioListener(result)
        }
    }

    private inner class ActualizarHorarioAsync: AsyncTask<Any, Unit, Int>() {

        override fun doInBackground(vararg params: Any?): Int {

            val horario = params[0] as Horario
            val diasSeleccionados = params[1] as List<*>
            val actualizarHorario: Boolean

            try {
                actualizarHorario = colicionaConOtrosHorarios(horario, dias)
            } catch (e: Exception) {
                return ERROR_HORA_DE_INICIO
            }

            if (actualizarHorario) {

                // actualiza el horario
                db.horarioDao().update(horario.nombre!!, horario.horaDeInicio!!, horario.horaDeFinalizacion!!, horario.horarioId!!)

                // Actualiza las alarmas
                eliminarAlarmas(horario)
                crearAlarmas(horario, diasSeleccionados)

                // Actualiza los dias del horario
                actualizarDias(horario.horarioId!!, diasSeleccionados)

                return HORARIO_ACTUALIZADO
            }

            return ERROR_HORARIO_EXISTENTE
        }

        override fun onPostExecute(result: Int) {
            agregarHorarioListener(result)
        }

        private fun eliminarAlarmas(horario: Horario) {

            val tarea = db.tareaDao().ultimaTarea(horario.horarioId!!)
            val dias = db.diasDao().dias(horario.horarioId!!)

            if (tarea.isFinalizada) return

            for (dia in dias) {

                val intent = Intent(app, TareasReceiver::class.java).apply { action = ACTION_EJECUTAR_TAREA }
                val pendingId = (tarea.tareaId!! * 7) + dia
                val pendingIntent = PendingIntent.getBroadcast(app, pendingId, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                val alarmManager = app.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager.cancel(pendingIntent)
            }
        }

        private fun crearAlarmas(horario: Horario, dias: List<*>) {

            val tarea = db.tareaDao().ultimaTarea(horario.horarioId!!)
            val horaDeInicio = horario.horaDeInicio!!.toTime()
            val diaActual = LocalDate.now().dayOfWeek.value
            val horaActual = LocalTime.now()

            for (dia in dias) {

                dia as NombreDia
                val nDia = dia.nombreDiaId!!.toLong()
                var fechaDeEjecucion: Long

                if ((nDia >= diaActual && horaDeInicio > horaActual) || nDia > diaActual) {

                    fechaDeEjecucion = LocalDateTime.now()
                            .with(ChronoField.DAY_OF_WEEK, nDia)
                            .withHour(horaDeInicio.hour)
                            .withMinute(horaDeInicio.minute)
                            .withSecond(0)
                            .toInstant(OffsetDateTime.now().offset) // Obtiene la zona horaria actual
                            .toEpochMilli()

                } else {

                    fechaDeEjecucion = LocalDateTime.now()
                            .plusWeeks(1)
                            .with(ChronoField.DAY_OF_WEEK, nDia)
                            .withHour(horaDeInicio.hour)
                            .withMinute(horaDeInicio.minute)
                            .withSecond(0)
                            .toInstant(OffsetDateTime.now().offset) // Obtiene la zona horaria actual
                            .toEpochMilli()
                }

                val intent = Intent(app, TareasReceiver::class.java).apply {
                    action = ACTION_EJECUTAR_TAREA
                    putExtra(EXTRA_TAREA, ParcelableUtil.marshall(tarea))
                }

                val pendingId = ((tarea.tareaId!! * 7) + nDia).toInt()
                val pendingIntent = PendingIntent.getBroadcast(app, pendingId, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                val alarmManager = app.getSystemService(Context.ALARM_SERVICE) as AlarmManager

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, fechaDeEjecucion, pendingIntent)
                } else {
                    alarmManager.setExact(AlarmManager.RTC, fechaDeEjecucion, pendingIntent)
                }
            }
        }

        /**
         * Actualiza los dias del horario
         *
         * Para actualizar los dias del horario primero elimina todos los dias
         * del horario almacenados de la base de datos y luego almacena los dias
         * seleccionados.
         * */
        private fun actualizarDias(horarioId: Int, diasSeleccionados: List<*>) {

            // Elimina los dia almacenados del horario
            db.diasDao().borrarDiasDelHorario(horarioId)

            // Almacena los dias seleccionados en la base de datos
            val dias = mutableListOf<Dia>()
            for (d in diasSeleccionados) {
                d as NombreDia
                dias.add(Dia(horarioId, d.nombreDiaId!!))
            }
            db.diasDao().insert(*dias.toTypedArray())
        }
    }

    fun colicionaConOtrosHorarios(horario: Horario, dias: List<*>): Boolean {

        val horarios = db.horarioDao().horariosDias().filter {

            if (horario.horarioId == it.horarioId) return@filter false

            var filtrar = false

            for1@ for (diaNuevoHorario in dias) {

                diaNuevoHorario as NombreDia

                for (diaHorario in it.dias!!) {

                    if (diaHorario.nombreDiaId == diaNuevoHorario.nombreDiaId) {
                        filtrar = true
                        break@for1
                    }
                }
            }

            return@filter filtrar

        }.sortedWith(Comparator { h1, h2 ->

            val horaDeInicio = h1.horaDeInicio?.toTime()
            val horaDeFinalizacion = h2.horaDeInicio?.toTime()

            return@Comparator horaDeInicio!!.compareTo(horaDeFinalizacion)
        })

        var crearHorario = false

        if (horario.horaDeInicio!!.toTime() > horario.horaDeFinalizacion!!.toTime()) {
            throw Exception()
        }

        if (horarios.isNotEmpty()) {

            if (horario.horaDeFinalizacion!!.toTime() < horarios[0].horaDeInicio!!.toTime()) {

                crearHorario = true

            } else {

                if (horarios.size > 1) {

                    var nHorarioAnterior = 0
                    var nHorarioSiguiente = 1

                    while (nHorarioSiguiente < horarios.size) {

                        val horarioAnterior = horarios[nHorarioAnterior]
                        val horarioSiguiente = horarios[nHorarioSiguiente]

                        if (horario.horaDeInicio!!.toTime() > horarioAnterior.horaDeFinalizacion!!.toTime()
                                && horario.horaDeFinalizacion!!.toTime() < horarioSiguiente.horaDeInicio!!.toTime()) {
                            crearHorario = true
                            break
                        } else {

                            if (nHorarioSiguiente == horarios.size - 1) {

                                if (horario.horaDeInicio!!.toTime() > horarios.last().horaDeFinalizacion!!.toTime()) {
                                    crearHorario = true
                                }
                            }
                        }

                        nHorarioAnterior++
                        nHorarioSiguiente++
                    }

                } else {

                    if (horario.horaDeInicio!!.toTime() > horarios.last().horaDeFinalizacion!!.toTime()) {
                        crearHorario = true
                    }
                }
            }

        } else {
            crearHorario = true
        }

        return crearHorario
    }
}