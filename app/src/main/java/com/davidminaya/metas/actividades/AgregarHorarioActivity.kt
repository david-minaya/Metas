package com.davidminaya.metas.actividades

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.davidminaya.metas.R
import com.davidminaya.metas.adapters.DiasAdapter
import com.davidminaya.metas.database.entities.NombreDia
import com.davidminaya.metas.models.AgregarHorarioViewModel
import kotlinx.android.synthetic.main.activity_agregar_horario.*
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import com.davidminaya.metas.customview.horario.concatenarDias
import com.davidminaya.metas.database.entities.Horario
import com.davidminaya.metas.database.entities.HorarioTareasDias
import com.davidminaya.metas.utils.*
import org.threeten.bp.LocalDate

/**
 * Creada por david minaya el 01/10/2018 11:52.
 *
 */
class AgregarHorarioActivity : AppCompatActivity() {

    private var isFirstTimeCreateActivity = true
    private lateinit var diasAdapter: DiasAdapter
    private lateinit var model: AgregarHorarioViewModel
    private val diasSeleccionados = mutableListOf<NombreDia>()
    private var horaDeInicio: LocalTime? = null
    private var horaDeFinalizacion: LocalTime? = null
    private var nombre = ""
    private var horarioTareaDias: HorarioTareasDias? = null
    private var isEditMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_horario)

        horarioTareaDias = intent.getParcelableExtra(EXTRA_HORARIO_TAREAS)
        isEditMode = horarioTareaDias != null

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_clear)

        model = ViewModelProviders.of(this)[AgregarHorarioViewModel::class.java]

        diasAdapter = DiasAdapter(this, this::onSelectedDias)
        dias.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        dias.adapter = diasAdapter
        dualTimeSelector.setOnTimeSelectorListener(this::onTimeSelectorListener)

        inicializarVistas()
    }

    /*
     * Inicializa las vistas de acuerdo al modo de inicio de la actividad
     * */
    private fun inicializarVistas() = if (isEditMode) {

        supportActionBar?.title = getString(R.string.editar_horario)

        for (dia in model.dias) {
            for (d in horarioTareaDias?.dias!!)
                if (d.nombreDiaId == dia.nombreDiaId) dia.isSelected = true
        }

        diasAdapter.dias = model.dias
        dualTimeSelector.setDefaultTime(LocalTime.parse(horarioTareaDias?.horario?.horaDeInicio),
                LocalTime.parse(horarioTareaDias?.horario?.horaDeFinalizacion))

    } else {

        if (isFirstTimeCreateActivity) {

            // Selecciona el dia actual de la semana
            val dayNumber = LocalDate.now().dayOfWeek.value
            model.dias[dayNumber - 1].isSelected = true
            diasAdapter.dias = model.dias

        } else {
            diasAdapter.dias = model.dias
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(HORA_DE_INICIO, horaDeInicio?.toString())
        outState?.putString(HORA_DE_FINALIZACION, horaDeFinalizacion?.toString())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        isFirstTimeCreateActivity = false

        horaDeInicio = LocalTime.parse(savedInstanceState?.getString(HORA_DE_INICIO))
        horaDeFinalizacion = LocalTime.parse(savedInstanceState?.getString(HORA_DE_FINALIZACION))

        dualTimeSelector.setDefaultTime(horaDeInicio!!, horaDeFinalizacion!!)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_agregar_tarea_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {

            android.R.id.home -> {
                finish()
                return true
            }

            R.id.aceptar -> {

                if (diasSeleccionados.isNullOrEmpty()) {

                    AlertDialog.Builder(this).apply{
                        setTitle(getString(R.string.dias_no_seleccionados))
                        setPositiveButton(getString(R.string.ok)) {dialog, _ -> dialog.dismiss() }
                        show()
                    }

                    return true
                }

                val f = DateTimeFormatter.ofPattern("HH:mm")
                val horario = Horario(nombre, horaDeInicio!!.format(f), horaDeFinalizacion!!.format(f))

                if (!isEditMode) {

                    model.insertHorario(horario, diasSeleccionados)
                    model.setAgregarHorarioListenerListener { codigo ->

                        when (codigo) {

                            AgregarHorarioViewModel.HORARIO_CREADO -> {
                                finish()
                            }

                            AgregarHorarioViewModel.ERROR_HORARIO_EXISTENTE -> {
                                AlertDialog.Builder(this).apply {
                                    setTitle(getString(R.string.titulo_error_crear_horario))
                                    setMessage(getString(R.string.mensaje_horario_existente))
                                    setPositiveButton(getString(R.string.aceptar)) { dialog, _ -> dialog.dismiss() }
                                    show()
                                }
                            }

                            AgregarHorarioViewModel.ERROR_HORA_DE_INICIO -> {
                                AlertDialog.Builder(this).apply {
                                    setTitle(getString(R.string.titulo_error_crear_horario))
                                    setMessage(getString(R.string.mensaje_hora_de_inicio))
                                    setPositiveButton(getString(R.string.aceptar)) { dialog, _ -> dialog.dismiss() }
                                    show()
                                }
                            }
                        }
                    }

                } else {

                    horario.horarioId = horarioTareaDias?.horario?.horarioId
                    model.actualizaHorario(horario, diasSeleccionados)

                    model.setAgregarHorarioListenerListener { codigo ->

                        when (codigo) {

                            AgregarHorarioViewModel.HORARIO_ACTUALIZADO -> {
                                finish()
                            }

                            AgregarHorarioViewModel.ERROR_HORARIO_EXISTENTE -> {
                                AlertDialog.Builder(this).apply {
                                    setTitle(getString(R.string.titulo_error_actualizar_horario))
                                    setMessage(getString(R.string.mensaje_horario_existente))
                                    setPositiveButton(getString(R.string.aceptar)) { dialog, _ -> dialog.dismiss() }
                                    show()
                                }
                            }

                            AgregarHorarioViewModel.ERROR_HORA_DE_INICIO -> {
                                AlertDialog.Builder(this).apply {
                                    setTitle(getString(R.string.titulo_error_actualizar_horario))
                                    setMessage(getString(R.string.mensaje_hora_de_inicio))
                                    setPositiveButton(getString(R.string.aceptar)) { dialog, _ -> dialog.dismiss() }
                                    show()
                                }
                            }
                        }
                    }
                }

                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun onSelectedDias(dia: NombreDia) {

        if (dia.isSelected) {
            diasSeleccionados.add(dia)
        } else {
            diasSeleccionados.remove(dia)
        }

        generarNombre()
    }

    private fun onTimeSelectorListener(startTime: LocalTime, endTime: LocalTime) {

        horaDeInicio = startTime
        horaDeFinalizacion = endTime

        generarNombre()
    }

    private fun generarNombre() {

        val h = DateTimeFormatter.ofPattern("h:mm")
        val m = DateTimeFormatter.ofPattern("a")

        val inicio = "${horaDeInicio?.format(h)} ${horaDeInicio?.format(m)?.toLowerCase()?.replace(".", "")?.replace(" ", "")}"
        val fin = "${horaDeFinalizacion?.format(h)} ${horaDeFinalizacion?.format(m)?.toLowerCase()?.replace(".", "")?.replace(" ", "")}"
        nombre = "${diasSeleccionados.concatenarDias()} de $inicio a $fin"

        texto.text = nombre
    }
}