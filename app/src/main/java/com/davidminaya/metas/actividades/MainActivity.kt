package com.davidminaya.metas.actividades

import android.app.NotificationChannel
import android.app.NotificationManager
import android.arch.lifecycle.ViewModelProviders
import android.content.*
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.PopupMenu
import android.view.*
import com.davidminaya.metas.*
import com.davidminaya.metas.receivers.TareasReceiver
import com.davidminaya.metas.database.entities.HorarioTareasDias
import com.davidminaya.metas.grafica.Estado
import com.davidminaya.metas.grafica.Horario
import com.davidminaya.metas.servicios.EjecutarTareaService
import com.davidminaya.metas.utils.*
import com.davidminaya.metas.models.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.threeten.bp.LocalDate
import java.lang.ref.WeakReference
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var model: MainViewModel
    private lateinit var servicio: EjecutarTareaService
    private var serviceIsConnect = false
    private var localReceiver = LocalReceiver()
    private lateinit var popupMenu: PopupMenu
    private var filtrarHorariosDelDiaActual = false
    private lateinit var preferencias: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        model = ViewModelProviders.of(this).get(MainViewModel::class.java)
        preferencias = getPreferences(0)
        filtrarHorariosDelDiaActual = preferencias.getBoolean(FILTRAR_HORARIOS, false)

        grafica.onClickHorarioListener = this::onClickHorarioListener
        grafica.post {
            grafica.actualizatFechaDeLaAplicacionListener(this::actualizarFechaDeLaAplicacionListener)
        }

        barraDeTareas.setOnClickCancelarTarea(this::onClickCancelarTarea)
        if (!EjecutarTareaService.isRunning) barraDeTareas.mostrarUIProximaTarea(model)

        if (savedInstanceState == null) {
            sendBroadcast(Intent(this, TareasReceiver::class.java).apply { action = ACTION_CREAR_ALARMAS })
        }

        crearCanalDeNotificaciones()
    }

    private fun crearCanalDeNotificaciones() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val mChannel = NotificationChannel("com.davidminaya.metas", "Metas", NotificationManager.IMPORTANCE_DEFAULT)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }
    }

    private fun onClickHorarioListener(horarioWithTareas: HorarioTareasDias) {
        val intent = Intent(this, HorarioActivity::class.java)
        intent.putExtra(EXTRA_HORARIO_ID, horarioWithTareas.horario?.horarioId)
        startActivity(intent)
    }

    private fun actualizarFechaDeLaAplicacionListener() {
        LlenarGraficaAsyncTask(this).execute()
    }

    @Suppress("UNUSED_PARAMETER")
    private fun onClickCancelarTarea(v: View) {

        AlertDialog.Builder(this).apply {
            setTitle(getString(R.string.cancelar_tarea))
            setMessage(getString(R.string.mensaje_cancelar_tarea))
            setNegativeButton(getString(R.string.cancelar)) { dialog, _ -> dialog.dismiss() }
            setPositiveButton(getString(R.string.aceptar)) { _, _ ->

                if (EjecutarTareaService.isRunning) {
                    servicio.cancelarTarea()
                }
            }
            show()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(FILTRAR_HORARIOS, filtrarHorariosDelDiaActual)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        filtrarHorariosDelDiaActual = savedInstanceState.getBoolean(FILTRAR_HORARIOS)
    }

    override fun onStart() {
        super.onStart()

        LlenarGraficaAsyncTask(this).execute()

        val intent = Intent(this, EjecutarTareaService::class.java)
        bindService(intent, connectWithService, 0)

        val filters = IntentFilter()
        filters.addAction(FILTER_TAREA_START)
        filters.addAction(FILTER_TAREA_PROGRESS)
        filters.addAction(FILTER_TAREA_DONE)
        filters.addAction(FILTER_TAREA_FINISH)

        LocalBroadcastManager.getInstance(this).registerReceiver(localReceiver, filters)
    }

    private val connectWithService = object: ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {

            val localBinder = service as EjecutarTareaService.LocalBinder
            servicio = localBinder.getService()
            serviceIsConnect = true

            val tarea = servicio.tarea
            barraDeTareas.textTitulo = servicio.tarea.nombre
            barraDeTareas.progressColor = tarea.color
            barraDeTareas.progress = servicio.progress

            barraDeTareas.mostrarUIEjecutarTarea()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            serviceIsConnect = false
        }
    }

    override fun onStop() {
        super.onStop()

        if (serviceIsConnect) {
            unbindService(connectWithService)
            serviceIsConnect = false
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(localReceiver)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu, menu)

        popupMenu = PopupMenu(this, anclaPopupMenu).apply {
            inflate(R.menu.menu_filtro)
            setOnMenuItemClickListener(this@MainActivity::onMenuItemClick)
        }

        if (!filtrarHorariosDelDiaActual) {

            val menuItem = popupMenu.menu.findItem(R.id.todos_los_horarios)
            menuItem.isChecked = true

        } else {

            val menuItem = popupMenu.menu.findItem(R.id.horarios_del_dia_actual)
            menuItem.isChecked = true
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId) {

            R.id.filtrar -> {
                popupMenu.show()
                true
            }

            R.id.agregar_horario -> {
                startActivity(Intent(this, AgregarHorarioActivity::class.java))
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onMenuItemClick(item: MenuItem): Boolean {

        grafica.isFilter = true

        when (item.itemId) {

            R.id.todos_los_horarios -> {

                item.isChecked = !item.isChecked
                filtrarHorariosDelDiaActual = false
                LlenarGraficaAsyncTask(this@MainActivity).execute()

                return true
            }

            R.id.horarios_del_dia_actual -> {

                item.isChecked = !item.isChecked
                filtrarHorariosDelDiaActual = true
                LlenarGraficaAsyncTask(this).execute()

                return true
            }

            else -> {
                return false
            }
        }
    }

    private class LlenarGraficaAsyncTask(mainActivity: MainActivity): AsyncTask<Unit, Unit, List<HorarioTareasDias>?>() {

        private val referencia = WeakReference(mainActivity)

        override fun doInBackground(vararg params: Unit?): List<HorarioTareasDias>? {

            val mainActivity = referencia.get() ?: return null
            if (mainActivity.isFinishing) return null

            val horariosWithTareas = mainActivity.model.getHorariosWithTareas

            if (mainActivity.filtrarHorariosDelDiaActual) {

                val filtro = mutableListOf<HorarioTareasDias>()
                val diaActual = LocalDate.now().dayOfWeek.value

                for (hwt in horariosWithTareas) {

                    for (dia in hwt.dias!!) {

                        if (dia.nombreDiaId == diaActual) {
                            filtro.add(hwt)
                        }
                    }
                }

                return filtro.sortedWith(OrdenarPorFechaDeEjecucion())
            }

            return horariosWithTareas.sortedWith(OrdenarPorFechaDeEjecucion())
        }

        override fun onPostExecute(result: List<HorarioTareasDias>?) {
            super.onPostExecute(result)

            val mainActivity = referencia.get() ?: return

            if (mainActivity.isFinishing) return

            val horarios = ArrayList<Horario>()
            var y = 36f

            if (result == null) return

            try {

                for (h in result) {
                    horarios.add(Horario(h, y))
                    y += 80f
                }

                mainActivity.grafica.estado = Estado.NORMAL

                if (!mainActivity.serviceIsConnect) {
                    mainActivity.barraDeTareas.mostrarUIProximaTarea(mainActivity.model)
                }

                if (horarios.isEmpty()) {
                    mainActivity.barraDeTareas.mostrarUIHola()
                    mainActivity.grafica.estado = Estado.VACIA
                }

            } catch (e: Exception) {

                mainActivity.grafica.estado = Estado.ERROR
                mainActivity.barraDeTareas.mostrarUIHola()
            }

            mainActivity.grafica.horarios = horarios

            if (mainActivity.serviceIsConnect) {
                mainActivity.grafica.configurarTareaActual(mainActivity.servicio.tarea.tareaId!!)
            }
        }
    }

    private inner class LocalReceiver: BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {

            when (intent?.action) {

                FILTER_TAREA_START -> {

                    val tarea = intent.getParcelableExtra<com.davidminaya.metas.database.entities.Tarea>(EXTRA_TAREA)
                    barraDeTareas.textTitulo = tarea.nombre
                    barraDeTareas.progressColor = tarea.color
                    barraDeTareas.progress = 0

                    barraDeTareas.mostrarUIEjecutarTarea()
                    grafica.configurarTareaActual(tarea.tareaId!!)
                }

                FILTER_TAREA_PROGRESS -> {

                    val progress = intent.getIntExtra(EXTRA_PROGRESS, 0)
                    barraDeTareas.progress = progress
                }

                FILTER_TAREA_DONE -> {

                    LlenarGraficaAsyncTask(this@MainActivity).execute()
                    barraDeTareas.mostrarUIProximaTarea(model)
                }

                FILTER_TAREA_FINISH -> {
                    barraDeTareas.mostrarUIProximaTarea(model)
                }
            }
        }
    }

    companion object {
        private const val FILTRAR_HORARIOS = "com.daviminaya.metas.FILTRAR_HORARIOS"
    }
}
