package com.davidminaya.metas.actividades

import android.animation.AnimatorInflater
import android.animation.ValueAnimator
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.*
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.davidminaya.metas.R
import com.davidminaya.metas.adapters.TareasAdapter
import com.davidminaya.metas.database.entities.HorarioTareasDias
import com.davidminaya.metas.database.entities.Tarea
import com.davidminaya.metas.utils.*
import com.davidminaya.metas.utils.toDate
import com.davidminaya.metas.models.HorarioActivityViewModel
import kotlinx.android.synthetic.main.activity_horario.*
import java.lang.ref.WeakReference

class HorarioActivity: AppCompatActivity() {

    private lateinit var model: HorarioActivityViewModel
    private var horarioId = -1
    private lateinit var horarioTareasDias: HorarioTareasDias

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_horario)

        model = ViewModelProviders.of(this)[HorarioActivityViewModel::class.java]
        horarioId = intent.getIntExtra(EXTRA_HORARIO_ID, 0)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tareas.layoutManager = LinearLayoutManager(this)
        agregarTarea.setOnClickListener(this::onClickAgregarTareaListener)
    }

    override fun onStart() {
        super.onStart()

        fantasma.visibility = View.GONE
        texto.visibility = View.GONE

        GetHorarioTareasDiasAsyncTask(this).execute(horarioId)
    }

    private class GetHorarioTareasDiasAsyncTask(horarioActivity: HorarioActivity): AsyncTask<Int, Unit, HorarioTareasDias>() {

        private val referencia = WeakReference(horarioActivity)

        override fun doInBackground(vararg params: Int?): HorarioTareasDias? {

            val activity = referencia.get() ?: return null
            if (activity.isFinishing) return null

            val horarioTareasDias = activity.model.horarioTareasDias(params[0]!!)

            horarioTareasDias.tareas = horarioTareasDias.tareas?.sortedWith(Comparator { t1, t2 ->

                val fechaDeInicio1 = t1.fechaDeInicio?.toDate()
                val fechaDeInicio2 = t2.fechaDeInicio?.toDate()

                return@Comparator fechaDeInicio2!!.compareTo(fechaDeInicio1)
            })

            return horarioTareasDias
        }

        override fun onPostExecute(result: HorarioTareasDias?) {

            val activity = referencia.get() ?: return
            if (activity.isFinishing || result == null) return

            activity.horarioTareasDias = result
            activity.supportActionBar?.title = activity.horarioTareasDias.horario?.nombre

            if (activity.horarioTareasDias.tareas!!.isEmpty()) {

                activity.fantasma.visibility = View.VISIBLE
                activity.texto.visibility = View.VISIBLE

                val fantasmaAnimator = AnimatorInflater.loadAnimator(activity, R.animator.fantasma_animator) as ValueAnimator
                fantasmaAnimator.addUpdateListener { activity.fantasma.y = it.animatedValue as Float + activity.toolbar.height }
                fantasmaAnimator.start()
            }

            activity.tareas.adapter = TareasAdapter(activity,
                    activity.horarioTareasDias.tareas ?: listOf(),
                    activity::onClickItemListener)
        }
    }

    private fun onClickItemListener(tarea: Tarea) {

        val intent = Intent(this, TareaActivity::class.java).apply {
            putExtra(EXTRA_TAREA, tarea)
            putExtra(EXTRA_NOMBRE_HORARIO, horarioTareasDias.horario?.nombre)
        }

        startActivity(intent)
    }

    private fun onClickAgregarTareaListener(@Suppress("UNUSED_PARAMETER") view: View) {

        val intent = Intent(this, AgregarTareaActivity::class.java).apply {
            putExtra(EXTRA_HORARIO_ID, horarioTareasDias.horario?.horarioId)
        }

        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_horario_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> {
                finish()
            }

            R.id.editar -> {

                val intent = Intent(this, AgregarHorarioActivity::class.java)
                intent.putExtra(EXTRA_HORARIO_TAREAS, horarioTareasDias)
                startActivity(intent)
            }

            R.id.eliminar -> {

                AlertDialog.Builder(this).apply {
                    setTitle(getString(R.string.eliminar_horario))
                    setMessage(getString(R.string.eliminar_horario_mensaje))
                    setNegativeButton(getString(R.string.cancelar)) { dialog, _ -> dialog.dismiss() }
                    setPositiveButton(getString(R.string.eliminar)) { _, _ ->
                        model.eliminarHorario(horarioTareasDias.horario!!)
                        model.horarioEliminadoListener {
                            finish()
                        }
                    }
                    show()
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }
}