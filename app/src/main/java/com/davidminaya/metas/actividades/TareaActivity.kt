package com.davidminaya.metas.actividades

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.davidminaya.metas.R
import com.davidminaya.metas.database.entities.Tarea
import com.davidminaya.metas.utils.*
import com.davidminaya.metas.models.TareaViewModel
import kotlinx.android.synthetic.main.activity_tarea.*
import org.threeten.bp.LocalDate
import org.threeten.bp.Period
import org.threeten.bp.format.DateTimeFormatter

class TareaActivity: AppCompatActivity() {

    private lateinit var tarea: Tarea
    private lateinit var model: TareaViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tarea)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        model = ViewModelProviders.of(this).get(TareaViewModel::class.java)

        tarea = intent.getParcelableExtra(EXTRA_TAREA)
        val nombreHorario = intent.getStringExtra(EXTRA_NOMBRE_HORARIO)

        if (savedInstanceState == null) {
            toolbar.title = tarea.nombre
            toolbar.setBackgroundColor(tarea.color)
            descripcion.text = tarea.descripcion
        }

        if (!tarea.isFinalizada) {

            tituloFechaDeFinalizacion.text = getString(R.string.proxima_ejecucion)
            estado.text = getString(R.string.en_ejecucion)
            model.proximaEjecucion(tarea, this, Observer { fechaDeFinalizacion.text = it })

        } else {

            fechaDeFinalizacion.text = formatearFecha(tarea.fechaDeFinalizacion)
        }

        if (tarea.descripcion.isNullOrBlank()) {
            tituloDescripcion.visibility = View.GONE
            descripcion.visibility = View.GONE
        }

        horario.text = nombreHorario
        fechaDeInicio.text = formatearFecha(tarea.fechaDeInicio)
        fechaDeFinalizacion.text = formatearFecha(tarea.fechaDeFinalizacion)
        duracion.text = calcularDuracion(tarea.fechaDeInicio?.toDate(), tarea.fechaDeFinalizacion?.toDate())
    }

    private fun formatearFecha(fechaString: String?): String {

        if (fechaString == null) return ""

        val fecha = LocalDate.parse(fechaString)
        val format = DateTimeFormatter.ofPattern("d 'de' MMMM 'del' YYYY ")

        return fecha.format(format)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(EXTRA_TAREA, tarea)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        tarea = savedInstanceState.getParcelable(EXTRA_TAREA)
        toolbar.title = tarea.nombre
        toolbar.setBackgroundColor(tarea.color)
        descripcion.text = tarea.descripcion
    }

    private fun calcularDuracion(fechaDeInicio: LocalDate?, fechaDeFinalizacion: LocalDate?): String {

        val periodo = Period.between(fechaDeInicio, fechaDeFinalizacion)

        var fecha = ""

        if (periodo.years > 0) fecha += "${periodo.years} ${getString(R.string.anios)}, "
        if (periodo.months > 0) fecha += "${periodo.months} ${getString(R.string.meses)} ${getString(R.string.y)} "

        fecha += "${periodo.days} ${getString(R.string.dias)}"

        return fecha
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_tarea_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {

            android.R.id.home -> {
                finish()
            }

            R.id.editar -> {

                val intent = Intent(this, AgregarTareaActivity::class.java).apply {
                    putExtra(EXTRA_IS_EDIT_MODE, true)
                    putExtra(EXTRA_TAREA, tarea)
                }

                startActivityForResult(intent, 0)
            }

            R.id.finalizar -> {

                AlertDialog.Builder(this).apply {
                    setTitle(getString(R.string.finalizar_tarea))
                    setNegativeButton(getString(R.string.cancelar)) { dialog, _ -> dialog.dismiss() }
                    setPositiveButton(getString(R.string.eliminar)) { _, _ ->
                        tarea.isFinalizada = true
                        model.actualizarTarea(tarea)
                    }
                    show()
                }
            }

            R.id.eliminar -> {

                AlertDialog.Builder(this).apply {
                    setTitle(getString(R.string.eliminar_tarea))
                    setNegativeButton(getString(R.string.cancelar)) { dialog, _ -> dialog.dismiss() }
                    setPositiveButton(getString(R.string.eliminar)) { _, _ ->
                        model.eliminarTarea(tarea)
                        finish()
                    }
                    show()
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 0) {

            if (data == null) return

            tarea = data.getParcelableExtra(EXTRA_TAREA)

            toolbar.title = tarea.nombre
            toolbar.setBackgroundColor(tarea.color)
            descripcion.text = tarea.descripcion
        }
    }
}