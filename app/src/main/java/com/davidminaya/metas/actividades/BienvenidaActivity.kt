package com.davidminaya.metas.actividades

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.View
import com.davidminaya.metas.BienvenidaFragment
import com.davidminaya.metas.R
import com.davidminaya.metas.utils.ES_LA_PRIMERA_EJECUCION
import com.davidminaya.metas.utils.START_ACTIVITY
import kotlinx.android.synthetic.main.activity_bienvenida.*

class BienvenidaActivity : AppCompatActivity() {

    private lateinit var preferencias: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bienvenida)

        preferencias = getSharedPreferences(START_ACTIVITY, Context.MODE_PRIVATE)

        viewPager.adapter = ViewPageAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager, true)
        tabLayout.addOnTabSelectedListener(onTabSelectedListener)
    }

    fun onClickOmitirListener(view: View) {

        preferencias.edit().apply {
            putBoolean(ES_LA_PRIMERA_EJECUCION, false)
            apply()
        }

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    fun onClickSiguienteListener(view: View) {

        if (viewPager.currentItem < 3) {

            viewPager.currentItem++

        } else {

            preferencias.edit().apply {
                putBoolean(ES_LA_PRIMERA_EJECUCION, false)
                apply()
            }

            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private val onTabSelectedListener = object: TabLayout.OnTabSelectedListener {

        override fun onTabSelected(tab: TabLayout.Tab?) {

            when (tab?.position) {

                0 -> {
                    botonOmitir.visibility = View.VISIBLE
                }

                1 -> {
                    botonOmitir.visibility = View.INVISIBLE
                }

                2 -> {
                    botonSiguiente.text = getString(R.string.siguiente)
                }

                3 -> {
                    botonSiguiente.text = getString(R.string.Finalizar)
                }
            }
        }

        override fun onTabReselected(tab: TabLayout.Tab?) {}

        override fun onTabUnselected(tab: TabLayout.Tab?) {}
    }

    private inner class ViewPageAdapter(supportFragmentManager: FragmentManager): FragmentStatePagerAdapter(supportFragmentManager) {

        override fun getItem(position: Int): Fragment {

            return when (position) {

                0 -> {
                    BienvenidaFragment.createNewInstance(R.drawable.icono_medio, getString(R.string.mensaje_de_bienvenida1), true)
                }

                1 -> {
                    BienvenidaFragment.createNewInstance(R.drawable.horario, getString(R.string.mensaje_de_bienvenida2))
                }

                2 -> {
                    BienvenidaFragment.createNewInstance(R.drawable.tareas, getString(R.string.mensaje_de_bienvenida3))
                }

                3 -> {
                    BienvenidaFragment.createNewInstance(R.drawable.historial, getString(R.string.mensaje_de_bienvenida4))
                }

                else -> {
                    BienvenidaFragment.createNewInstance(R.drawable.fantasma_error, getString(R.string.mensaje_pantalla_prohivida))
                }
            }
        }

        override fun getCount(): Int = 4
    }
}
