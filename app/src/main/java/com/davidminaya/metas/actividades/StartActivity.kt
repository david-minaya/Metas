package com.davidminaya.metas.actividades

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.davidminaya.metas.utils.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val preferencias = getSharedPreferences(START_ACTIVITY, Context.MODE_PRIVATE)
        val esLaPrimeraEjecucion = preferencias.getBoolean(ES_LA_PRIMERA_EJECUCION, true)

        if (!esLaPrimeraEjecucion) {
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            startActivity(Intent(this, BienvenidaActivity::class.java))
        }

        finish()
    }
}
