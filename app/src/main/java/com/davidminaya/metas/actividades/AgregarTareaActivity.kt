package com.davidminaya.metas.actividades

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_agregar_tarea.*
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import com.davidminaya.metas.adapters.ColorsAdapter
import com.davidminaya.metas.adapters.HorariosSpinnerAdapter
import com.davidminaya.metas.database.entities.Tarea
import com.davidminaya.metas.models.TareaViewModel
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import com.davidminaya.metas.R
import com.davidminaya.metas.utils.*

/**
 * Creada por david minaya el 17/09/2018 12:15.
 */
class AgregarTareaActivity : AppCompatActivity() {

    companion object {
        private const val KEY_POSITION = "position"
    }

    private lateinit var colorsAdapter: ColorsAdapter
    private var position = 0
    private lateinit var horariosSpinnerAdapter: HorariosSpinnerAdapter

    private lateinit var viewModel: TareaViewModel
    private var isEditMode = false
    private var tarea = Tarea()
    private var horarioId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_tarea)

        isEditMode = intent.getBooleanExtra(EXTRA_IS_EDIT_MODE, false)

        if (isEditMode) {
            tarea = intent.getParcelableExtra(EXTRA_TAREA)
        } else {
            horarioId = intent.getIntExtra(EXTRA_HORARIO_ID, 0)
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_clear)

        viewModel = ViewModelProviders.of(this)[TareaViewModel::class.java]
        viewModel.horarios.observe(this, Observer { onChange(it) })

        horariosSpinnerAdapter = HorariosSpinnerAdapter(this)
        horario.adapter = horariosSpinnerAdapter
        horario.onItemSelectedListener = itemSelectedListener

        colorsAdapter = ColorsAdapter(this)
        colorsAdapter.onItemClick = this::itemClick
        colores.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        colores.adapter = colorsAdapter

        if (isEditMode) {

            toolbar.title = getString(R.string.editar_tarea)
            nombre.setText(tarea.nombre)
            descripcion.setText(tarea.descripcion)
            colorsAdapter.setSelectItemColor(tarea.color)
        }
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_POSITION, position)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        position = savedInstanceState.getInt(KEY_POSITION, 0)
        colorsAdapter.setLastPosition(position)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_agregar_tarea_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {

            android.R.id.home -> {
                finish()
                return true
            }

            R.id.aceptar -> {

                if (nombre.text.isBlank()) {
                    nombre.error = getString(R.string.mensaje_error_edittext_nombre)
                    return true
                }

                tarea.nombre = nombre.text.toString()
                tarea.descripcion = descripcion.text.toString()

                if (!isEditMode) {

                    tarea.fechaDeInicio = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)
                    tarea.fechaDeFinalizacion = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)

                    viewModel.setOnCrearTareaListener { codigo ->

                        when (codigo) {

                            TareaViewModel.TAREA_CREADA -> {
                                finish()
                            }

                            TareaViewModel.TAREA_NO_CREADA -> {

                                AlertDialog.Builder(this).apply {
                                    setTitle(getString(R.string.error_crear_tarea))
                                    setMessage(getString(R.string.mensaje_error_crear_tarea))
                                    setPositiveButton(getString(R.string.ok)) { dialog, _ -> dialog.dismiss() }
                                    show()
                                }
                            }
                        }
                    }

                    viewModel.saveTarea(tarea)

                } else {

                    viewModel.actualizarTarea(tarea)

                    val intent = Intent().apply { putExtra(EXTRA_TAREA, tarea) }
                    setResult(0, intent)

                    finish()
                }

                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun onChange(horarios: MutableList<com.davidminaya.metas.database.entities.Horario>?) {

        if (horarios != null) {

            horariosSpinnerAdapter.horarios = horarios

            val position = if (isEditMode) {
                horariosSpinnerAdapter.getPosition(tarea.horarioId)
            } else {
                horariosSpinnerAdapter.getPosition(horarioId)
            }

            horario.setSelection(position)
        }
    }

    private val itemSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            tarea.horarioId = horariosSpinnerAdapter.horarios[position].horarioId
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }

    /*
     * Evento onClick de la lista de colores
     */
    private fun itemClick(color: Int, i: Int) {

        tarea.color = color

        // Almacena la posicion del item seleccionado para cuando se
        // restaure la actividad
        position = i
    }
}