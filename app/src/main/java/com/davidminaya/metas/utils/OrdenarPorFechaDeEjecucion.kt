package com.davidminaya.metas.utils

import com.davidminaya.metas.database.entities.HorarioTareasDias
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.temporal.ChronoField

class OrdenarPorFechaDeEjecucion: Comparator<HorarioTareasDias> {

    override fun compare(h1: HorarioTareasDias, h2: HorarioTareasDias): Int {
        return fechaDeEjecucion(h1)?.compareTo(fechaDeEjecucion(h2)) ?: 0
    }

    private fun fechaDeEjecucion(horarioTareasDias: HorarioTareasDias): LocalDateTime? {

        var fechaDeEjecucionProximaTarea: LocalDateTime? = null
        val fechaActual = LocalDateTime.now().withHour(0).withMinute(0)
        val diaActual = fechaActual.dayOfWeek.value
        val horaActual = fechaActual.toLocalTime()
        val dias = horarioTareasDias.dias!!
        val horaDeInicio = LocalTime.parse(horarioTareasDias.horario?.horaDeInicio)

        for (dia in dias) {

            val nDia = dia.nombreDiaId!!.toLong()
            var fechaDeEjecucion: LocalDateTime

            if ((nDia >= diaActual && horaDeInicio > horaActual) || nDia > diaActual) {

                fechaDeEjecucion = LocalDateTime.now()
                        .with(ChronoField.DAY_OF_WEEK, nDia)
                        .withHour(horaDeInicio.hour)
                        .withMinute(horaDeInicio.minute)
                        .withSecond(0)

            } else {

                fechaDeEjecucion = LocalDateTime.now()
                        .plusWeeks(1)
                        .with(ChronoField.DAY_OF_WEEK, nDia)
                        .withHour(horaDeInicio.hour)
                        .withMinute(horaDeInicio.minute)
                        .withSecond(0)
            }

            if (fechaDeEjecucionProximaTarea == null) {
                fechaDeEjecucionProximaTarea = fechaDeEjecucion
            }

            if (fechaDeEjecucion < fechaDeEjecucionProximaTarea) {
                fechaDeEjecucionProximaTarea = fechaDeEjecucion
            }
        }

        return fechaDeEjecucionProximaTarea
    }
}