package com.davidminaya.metas.utils

import android.graphics.Color
import android.support.annotation.ColorInt
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime

fun String.toTime(): LocalTime = LocalTime.parse(this)
fun String.toDate(): LocalDate = LocalDate.parse(this)

/**
 * Transparenta un color.
 *
 * 0 = transparente
 * 100 = opaco
 *
 * @param color color que se va a transparentar.
 * @param porcentaje transparencia que se le aplicara al color. La
 * opacidad va desde 0 hasta 100. Si la opacidad es mayor o menor
 * a estos numeros se aplica una opacidad de 100.
 *
 * @return retorna el color transparentado
 */
fun transparentar(color: Int, porcentaje: Int): Int {

    @ColorInt val r = Color.red(color)
    @ColorInt val g = Color.green(color)
    @ColorInt val b = Color.blue(color)

    var opacidad = porcentaje * (255 / 100)

    if (porcentaje < 0 || porcentaje > 100) opacidad = 100

    return Color.argb(opacidad, r, g, b)
}