package com.davidminaya.metas.customview

import android.content.Context
import android.os.AsyncTask
import android.support.constraint.ConstraintLayout
import android.support.transition.TransitionManager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.davidminaya.metas.R
import com.davidminaya.metas.models.MainViewModel
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.TextStyle
import org.threeten.bp.temporal.ChronoField
import org.threeten.bp.temporal.WeekFields
import java.lang.ref.WeakReference
import java.util.*

class BarraDeTareas : ConstraintLayout {

    private lateinit var titulo: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var cancelar: TextView
    private lateinit var tituloProximaTarea: TextView
    private lateinit var proximaTarea: TextView
    private lateinit var fechaDeEjecucion: TextView
    private lateinit var hola: TextView

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {

        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        layoutInflater.inflate(R.layout.barra_de_tareas, this, true)

        // UI ejecutar tarea
        titulo = findViewById(R.id.titulo)
        progressBar = findViewById(R.id.progressbar)
        cancelar = findViewById(R.id.cancelar)

        // UI proxima tarea
        tituloProximaTarea = findViewById(R.id.tituloProximaTarea)
        proximaTarea = findViewById(R.id.proximaTarea)
        fechaDeEjecucion = findViewById(R.id.fechaDeEjecucion)

        // UI hola
        hola = findViewById(R.id.hola)

        // Oculta todas la UI
        titulo.visibility = View.GONE
        progressBar.visibility = View.GONE
        cancelar.visibility = View.GONE
        tituloProximaTarea.visibility = View.GONE
        proximaTarea.visibility = View.GONE
        fechaDeEjecucion.visibility = View.GONE
        hola.visibility = View.GONE
    }

    var textTitulo: String = ""
        set(value) {
            titulo.text = value
        }

    var progressColor: Int = 0
        set(value) {
            progressBar.setColorProgress(value)
        }

    var progress: Int = 0
        set(value) {
            progressBar.progress = value
        }

    var textProximaTarea: String = ""
        set(value) {
            proximaTarea.text = value
        }

    var textFechaDeEjecucion = ""
        set(value) {
            fechaDeEjecucion.text = value
        }

    fun setOnClickCancelarTarea(onClickListener: (View) -> Unit) {
        cancelar.setOnClickListener(onClickListener)
    }

    fun mostrarUIEjecutarTarea() {

        TransitionManager.beginDelayedTransition(this)

        tituloProximaTarea.visibility = View.GONE
        proximaTarea.visibility = View.GONE
        fechaDeEjecucion.visibility = View.GONE
        hola.visibility = View.GONE

        titulo.visibility = View.VISIBLE
        progressBar.visibility = View.VISIBLE
        cancelar.visibility = View.VISIBLE

    }

    fun mostrarUIProximaTarea(model: MainViewModel) {
        ConsultarProximaTareaAsyncTask(this, model).execute()
    }

    fun mostrarUIHola() {

        TransitionManager.beginDelayedTransition(this)

        titulo.visibility = View.GONE
        progressBar.visibility = View.GONE
        cancelar.visibility = View.GONE
        tituloProximaTarea.visibility = View.GONE
        proximaTarea.visibility = View.GONE
        fechaDeEjecucion.visibility = View.GONE

        hola.visibility = View.VISIBLE
    }

    private class ConsultarProximaTareaAsyncTask(
            barraDeTareas: BarraDeTareas,
            private val model: MainViewModel): AsyncTask<Unit, Unit, Pair<String, String>>() {

        private val reference: WeakReference<BarraDeTareas> = WeakReference(barraDeTareas)
        private lateinit var hoy: String
        private lateinit var manana: String
        private lateinit var aLas: String
        private lateinit var proximo: String

        override fun onPreExecute() {
            super.onPreExecute()

            val barra = reference.get() ?: return

            hoy = barra.context.getString(R.string.hoy)
            manana = barra.context.getString(R.string.manana)
            aLas = barra.context.getString(R.string.a_las)
            proximo = barra.context.getString(R.string.proximo)
        }

        override fun doInBackground(vararg params: Unit?): Pair<String, String>? {

            var nombreProximaTarea = ""
            var fechaDeEjecucionProximaTarea: LocalDateTime? = null
            val fechaActual = LocalDateTime.now()
            val diaActual = fechaActual.dayOfWeek.value
            val horaActual = fechaActual.toLocalTime()
            val horariosTareasDias = model.getHorariosTareasDias

            for (horarioTareaDias in horariosTareasDias) {

                val dias = horarioTareaDias.dias!!
                val horaDeInicio = LocalTime.parse(horarioTareaDias.horario?.horaDeInicio)

                if (horarioTareaDias.tarea!!.isFinalizada) continue

                for (dia in dias) {

                    val nDia = dia.nombreDiaId!!.toLong()
                    var fechaDeEjecucion: LocalDateTime

                    if ((nDia >= diaActual && horaDeInicio > horaActual) || nDia > diaActual) {

                        fechaDeEjecucion = LocalDateTime.now()
                                .with(ChronoField.DAY_OF_WEEK, nDia)
                                .withHour(horaDeInicio.hour)
                                .withMinute(horaDeInicio.minute)
                                .withSecond(0)

                    } else {

                        fechaDeEjecucion = LocalDateTime.now()
                                .plusWeeks(1)
                                .with(ChronoField.DAY_OF_WEEK, nDia)
                                .withHour(horaDeInicio.hour)
                                .withMinute(horaDeInicio.minute)
                                .withSecond(0)
                    }

                    if (fechaDeEjecucionProximaTarea == null) {
                        fechaDeEjecucionProximaTarea = fechaDeEjecucion
                        nombreProximaTarea = horarioTareaDias.tarea!!.nombre
                    }

                    if (fechaDeEjecucion < fechaDeEjecucionProximaTarea) {
                        fechaDeEjecucionProximaTarea = fechaDeEjecucion
                        nombreProximaTarea = horarioTareaDias.tarea!!.nombre
                    }
                }
            }

            if (fechaDeEjecucionProximaTarea == null) return null

            val semana = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear()
            val h = DateTimeFormatter.ofPattern("h:mm")
            val m = DateTimeFormatter.ofPattern("a")

            val esHoy = fechaActual.dayOfMonth == fechaDeEjecucionProximaTarea.dayOfMonth
            val esManana = fechaActual.plusDays(1).dayOfMonth == fechaDeEjecucionProximaTarea.dayOfMonth
            val esEnEstaSemana = fechaActual.get(semana) == fechaDeEjecucionProximaTarea.get(semana)

            var nombreDia = fechaDeEjecucionProximaTarea.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.forLanguageTag("es"))
            val hora = "${fechaDeEjecucionProximaTarea.format(h)} ${fechaDeEjecucionProximaTarea.format(m)?.toLowerCase()?.replace(".", "")?.replace(" ", "")}"

            nombreDia = nombreDia.substring(0, 1).capitalize() + nombreDia.substring(1, nombreDia.length)

            val proximaEjecucionString = when {
                esHoy -> "$hoy $aLas $hora"
                esManana -> "$manana $aLas $hora"
                !esHoy && !esManana && esEnEstaSemana -> "$nombreDia $aLas $hora"
                else -> "$proximo $nombreDia $aLas $hora"
            }

            return Pair(nombreProximaTarea, proximaEjecucionString)
        }

        override fun onPostExecute(result: Pair<String, String>?) {

            val barraDeTareas = reference.get() ?: return
            if (result == null) return

            barraDeTareas.textProximaTarea = result.first
            barraDeTareas.textFechaDeEjecucion = result.second

            TransitionManager.beginDelayedTransition(barraDeTareas)

            barraDeTareas.titulo.visibility = View.GONE
            barraDeTareas.progressBar.visibility = View.GONE
            barraDeTareas.cancelar.visibility = View.GONE
            barraDeTareas.hola.visibility = View.GONE

            barraDeTareas.tituloProximaTarea.visibility = View.VISIBLE
            barraDeTareas.proximaTarea.visibility = View.VISIBLE
            barraDeTareas.fechaDeEjecucion.visibility = View.VISIBLE
        }
    }
}
