package com.davidminaya.metas.customview.horario

import android.graphics.*
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * Creada por david minaya el 10/10/2018 11:46.
 *
 */
class Manecilla() {

    companion object {
        var dp = 0f
    }

    private var meridiano = ""
    var radio = 0f
    private var bounds: RectF
    var isTouch = false
    private var oldHora = -1
    private var oldMinuto = -1
    var tiempo: LocalTime? = null

    var radianes = 0.0
        set(value) {
            if (isTouch) {
                field = value
                calcularTiempo()
            }
        }

    private val lineaPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val circuloPaint: Paint
    private val meridianoPaint: Paint

    init {

        lineaPaint.style = Paint.Style.STROKE
        lineaPaint.strokeWidth = 1.5f * dp
        lineaPaint.color = Color.WHITE

        circuloPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        circuloPaint.style = Paint.Style.FILL
        circuloPaint.color = Color.WHITE

        meridianoPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        meridianoPaint.color = Color.BLACK
        meridianoPaint.textSize = 14 * dp
        meridianoPaint.textAlign = Paint.Align.CENTER
        meridianoPaint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)

        bounds = RectF()
    }

    constructor(time: LocalTime): this() {
        setTime(time)
    }

    constructor(radianes: Double, meridiano: String): this() {

        isTouch = true
        this.radianes = radianes
        this.meridiano = meridiano
        isTouch = false
    }

    fun setTime(time: LocalTime) {

        // Convierte la hora del formato de 24 horas al formato de 12 horas
        var hora = if (time.hour <= 11) time.hour else time.hour - 12

        // Ajusta la hora para que coincida con el desplazamiento de los grados
        // en el selector
        hora -= 3

        // Convierte la hora a radianes
        var calcRadianes = (hora * UNA_HORA) + (time.minute * (UNA_HORA / 60))

        // Convierte el rango de valores de -3.14/3.14 a 0/6.28
        calcRadianes = if (calcRadianes < 0.0) DOCE_HORAS + calcRadianes else calcRadianes

        // Convierte los radianes a un rango de progreso de 0.04363323129985823942309226921222
        // que equivales a un rango de progresos de 5 minutos
        val entre5Minutos = (calcRadianes / CINCO_MINUTOS).toInt()
        val por5Minutos = entre5Minutos * CINCO_MINUTOS
        calcRadianes = por5Minutos

        isTouch = true
        meridiano = time.format(DateTimeFormatter.ofPattern("a")).toLowerCase().replace(".", "").replace(" ", "")
        radianes = calcRadianes
        isTouch = false
    }

    fun draw(c: Canvas) {

        val x = (Math.cos(radianes) * radio).toFloat()
        val y = (Math.sin(radianes) * radio).toFloat()

        val radio = 14 * dp
        c.drawLine(0f, 0f, x, y, lineaPaint)
        c.drawCircle(x, y, radio, circuloPaint)
        c.drawText(meridiano, x, y + (5 * dp), meridianoPaint)

        bounds.set(x - (radio * 1.5f), y - (radio * 1.5f), x + (radio * 1.5f), y + (radio * 1.5f))
    }

    fun isTouch(x: Float, y: Float) {

        isTouch = x > bounds.left && x < bounds.right &&
                y > bounds.top && y < bounds.bottom
    }

    fun cambiarMeridiano() {

        if (isTouch) {

            meridiano = if (meridiano == "am") {
                "pm"
            } else {
                "am"
            }

            calcularTiempo()
        }
    }

    private fun calcularTiempo() {

        var hora = calcularHora()
        val minuto = calcularMinuto()

        // Convierte la hora de am a pm
        hora = if (meridiano == "pm") {
            if (hora == 12) 12 else hora + 12
        } else {
            if (hora == 12) 0 else hora
        }

        if (hora != oldHora || minuto != oldMinuto) {

            tiempo = LocalTime.of(hora, minuto)

            oldHora = hora
            oldMinuto = minuto
        }

    }

    private fun calcularHora(): Int {

        fun evaluarHoras(radianes: Double, n: Int): Boolean {

            val margenInferior = 0.0005
            val margenSuperior = UNA_HORA
            val hora = UNA_HORA * n

            if (radianes > hora - margenInferior && radianes < hora + margenSuperior) {
                return true
            }

            return false
        }

        return when {
            evaluarHoras(radianes, 9) -> 12
            evaluarHoras(radianes, 10) -> 1
            evaluarHoras(radianes, 11) -> 2
            evaluarHoras(radianes, 0) -> 3
            evaluarHoras(radianes, 1) -> 4
            evaluarHoras(radianes, 2) -> 5
            evaluarHoras(radianes, 3) -> 6
            evaluarHoras(radianes, 4) -> 7
            evaluarHoras(radianes, 5) -> 8
            evaluarHoras(radianes, 6) -> 9
            evaluarHoras(radianes, 7) -> 10
            evaluarHoras(radianes, 8) -> 11
            else -> 0
        }
    }

    private fun calcularMinuto(): Int {

        fun evaluarMinutos(_i: Double): Boolean {

            val margen = 0.0005

            var i = _i
            while (i <= DOCE_HORAS) {

                if (i > radianes - margen && i < radianes + margen) {
                    return true
                }

                i += UNA_HORA
            }

            return false
        }

        return when {
            evaluarMinutos(0.0) -> 0
            evaluarMinutos(CINCO_MINUTOS) -> 5
            evaluarMinutos(CINCO_MINUTOS * 2.0) -> 10
            evaluarMinutos(CINCO_MINUTOS * 3.0) -> 15
            evaluarMinutos(CINCO_MINUTOS * 4.0) -> 20
            evaluarMinutos(CINCO_MINUTOS * 5.0) -> 25
            evaluarMinutos(CINCO_MINUTOS * 6.0) -> 30
            evaluarMinutos(CINCO_MINUTOS * 7.0) -> 35
            evaluarMinutos(CINCO_MINUTOS * 8.0) -> 40
            evaluarMinutos(CINCO_MINUTOS * 9.0) -> 45
            evaluarMinutos(CINCO_MINUTOS * 10.0) -> 50
            evaluarMinutos(CINCO_MINUTOS * 11.0) -> 55
            else -> 0
        }
    }
}