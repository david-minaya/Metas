package com.davidminaya.metas.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

import com.davidminaya.metas.R
import com.davidminaya.metas.utils.transparentar

/**
 * Creada por david minaya el 25/05/2018 4:33.
 */
@Suppress("LiftReturnOrAssignment")
class ProgressBar : View {

    // Barra de progreso
    private var rectF: RectF? = null
    private var paint: Paint? = null
    private var progressBar: Path? = null

    // Lados
    private var mTop: Int = 0
    private var mLeft: Int = 0
    private var mBottom: Int = 0

    // Esquinas
    private val corners = FloatArray(8)

    // Atributos
    private var colorProgress = Color.RED
    var progress = 40
        set(value) {
            field = value
            invalidate()
        }

    private var disableBackground = false
    private var mBackgroundColor = transparentar(Color.WHITE, 40)

    private var dp = 0f

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    /**
     * Inicializa las variables que se utilizaran para dibujar la vista.
     * @param atributos atributos xml de la vista.
     */
    private fun init(atributos: AttributeSet?) {

        dp = resources.displayMetrics.density

        // Obtienes los atributos xml de la vista si existen
        if (atributos != null) {

            val typedArray = context.theme.obtainStyledAttributes(atributos, R.styleable.ProgressBar, 0, 0)

            try {
                progress = typedArray.getInt(R.styleable.ProgressBar_progress, 40)
                colorProgress = typedArray.getColor(R.styleable.ProgressBar_progress_color, Color.WHITE)
                disableBackground = typedArray.getBoolean(R.styleable.ProgressBar_disable_background, false)
                mBackgroundColor = transparentar(colorProgress, 40)
                isTaskFinish = typedArray.getBoolean(R.styleable.ProgressBar_is_task_finish, true)

            } finally {
                typedArray.recycle()
            }
        }

        // Inicializa las figuras y los estilos del dibujo
        rectF = RectF()
        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint!!.color = colorProgress
        progressBar = Path()
    }

    /**
     * Modifica el color de la barra de progreso.
     * @param color color de la barra de progreso.
     */
    fun setColorProgress(color: Int) {
        paint!!.color = color
        mBackgroundColor = transparentar(color, 40)
        invalidate()
    }

    var isTaskFinish: Boolean = false
        set(value) {
            field = value
            invalidate()
        }

    override fun onDraw(canvas: Canvas) {

        if (!disableBackground) {
            canvas.drawColor(mBackgroundColor)
        }

        // Calcula el 1% del ancho de la vista. Este valor es utilizado para calcular
        // el progreso de la barra de progreso.
        val porciento = width.toFloat() / 100
        var progressValue = progress * porciento

        // Si el progreso de la barra es mayor o igual a 100, se establece el progreso
        // en 100% y se eliminan las equinas redondeadas.
        if (progressValue >= width) {
            progressValue = 100 * porciento
            for (i in 2..5) corners[i] = 0f
        } else {
            val curvatura = height / 2f
            for (i in 2..5) corners[i] = curvatura
        }

        // Barra de progreso
        rectF!!.top = mTop.toFloat()
        rectF!!.left = mLeft.toFloat()
        rectF!!.right = progressValue
        rectF!!.bottom = mBottom.toFloat()
        progressBar!!.reset()
        progressBar!!.addRoundRect(rectF, corners, Path.Direction.CCW)

        canvas.drawPath(progressBar!!, paint!!)

        if (!isTaskFinish) {
            canvas.drawCircle(progressValue + (8 * dp), height / 2f, (2 * dp), paint)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        mLeft = paddingStart - width
        mBottom = h - paddingBottom
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val widthMin = suggestedMinimumWidth + paddingStart + paddingEnd
        val heightMin = suggestedMinimumHeight + paddingTop + paddingBottom + 12

        var width = 0
        var height = 0


        if (widthMode == View.MeasureSpec.EXACTLY) width = widthSize
        if (heightMode == View.MeasureSpec.EXACTLY) height = heightSize

        if (widthMode == View.MeasureSpec.AT_MOST) width = Math.min(widthSize, widthMin)
        if (heightMode == View.MeasureSpec.AT_MOST) height = Math.min(heightSize, heightMin)

        if (widthMode == View.MeasureSpec.UNSPECIFIED) width = widthMin
        if (heightMode == View.MeasureSpec.UNSPECIFIED) height = heightMin

        setMeasuredDimension(width, height)
    }
}