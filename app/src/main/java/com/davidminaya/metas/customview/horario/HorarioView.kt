package com.davidminaya.metas.customview.horario

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.Color.BLACK
import android.graphics.Color.WHITE
import android.support.v4.content.ContextCompat
import android.support.v4.view.GestureDetectorCompat
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import com.davidminaya.metas.R
import org.threeten.bp.LocalTime

const val DOCE_HORAS = Math.PI * 2
const val UNA_HORA = DOCE_HORAS / 12
const val CINCO_MINUTOS = UNA_HORA / 12

/**
 * Creada por david minaya el 03/10/2018 11:33.
 */
class HorarioView : View {

    private var dp = 0f
    private var radio = 0f
    private var onScrollIsNoRunning = true
    private var oldStartTime: LocalTime? = null
    private var oldEndTime: LocalTime? = null

    private lateinit var baseCirclePaint: Paint
    private lateinit var pointCenterPaint: Paint
    private lateinit var lineasPaint: Paint
    private lateinit var numerosPaint: Paint
    private lateinit var paintArc: Paint

    private lateinit var manecilla1: Manecilla
    private lateinit var manecilla2: Manecilla

    private lateinit var gesture: GestureDetectorCompat
    private lateinit var onTimeSelectorListener: (LocalTime, LocalTime) -> Unit

    constructor(context: Context) : super(context) { init(context) }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) { init(context) }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) { init(context) }

    private fun init(context: Context) {

        dp = resources.displayMetrics.density

        baseCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        baseCirclePaint.style = Paint.Style.FILL
        baseCirclePaint.color = ContextCompat.getColor(context, R.color.black_ligth)

        pointCenterPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        pointCenterPaint.style = Paint.Style.FILL
        pointCenterPaint.color = WHITE

        lineasPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        lineasPaint.style = Paint.Style.STROKE
        lineasPaint.color = ContextCompat.getColor(context, R.color.white)
        lineasPaint.strokeWidth = dp

        numerosPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        numerosPaint.textSize = 16 * dp
        numerosPaint.textAlign = Paint.Align.CENTER
        numerosPaint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)

        paintArc = Paint(Paint.ANTI_ALIAS_FLAG)
        paintArc.style = Paint.Style.FILL
        paintArc.color = Color.WHITE

        Manecilla.dp = dp
        manecilla1 = if(isInEditMode) Manecilla(0.0, "am") else Manecilla(LocalTime.now())
        manecilla2 = if(isInEditMode) Manecilla(Math.PI * 0.5, "pm") else Manecilla(LocalTime.now().plusHours(1))

        gesture = GestureDetectorCompat(context, simpleGesture)
    }

    fun setOnTimeSelectorListener(timeSelector: (LocalTime, LocalTime) -> Unit) {
        onTimeSelectorListener = timeSelector
        lanzarEventoTimeSelector()
    }

    fun setDefaultTime(horaDeInicio: LocalTime, horaDeFinalizacion: LocalTime) {
        manecilla1.setTime(horaDeInicio)
        manecilla2.setTime(horaDeFinalizacion)
        lanzarEventoTimeSelector()
    }

    override fun onDraw(c: Canvas?) {
        super.onDraw(c)

        if (c == null) return

        c.save()
        c.translate(width / 2f, height / 2f)

        // Circulo base
        c.drawCircle(0f, 0f, radio, baseCirclePaint)

        dibujarArc(c)

        dibujarLineas(c, 0f, 1f, 11, 15 * dp) // Horas
        dibujarLineas(c, 0.5f, 1f, 11, 10 * dp) // Medias horas
        dibujarLineas(c, 0.25f, .5f, 23, 5 * dp) // Cuartos de hora
        dibujarNumeros(c)

        manecilla1.draw(c)
        manecilla2.draw(c)

        // Punto central
        c.drawCircle(0f, 0f, 4 * dp, pointCenterPaint)

        c.restore()
    }

    private fun dibujarLineas(c: Canvas, start: Float, margen: Float, cantidad: Int, width: Float) {

        var j = start

        for (i in  0..cantidad) {

            val radianes = j * ((Math.PI * 2) / 12)
            val grados = (radianes * (180 / Math.PI)).toFloat()
            val x = (Math.cos(radianes) * (radio * 0.99)).toFloat()
            val y = (Math.sin(radianes) * (radio * 0.99)).toFloat()
            elegirColor(lineasPaint, radianes)

            c.save()
            c.translate(x, y)
            c.rotate(grados)
            c.drawLine(-width, 0f, 0f, 0f, lineasPaint)
            c.restore()

            j += margen
        }
    }

    private fun dibujarNumeros(c: Canvas) {

        val horas = arrayOf("3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "1", "2")

        for (i in 0..11) {

            val radianes = i * ((Math.PI * 2) / 12)
            val x = (Math.cos(radianes) * (radio * 0.78)).toFloat()
            val y = (Math.sin(radianes) * (radio * 0.78)).toFloat()
            elegirColor(numerosPaint, radianes)

            c.save()
            c.translate(x, y)
            c.drawText(horas[i], 0f, 6 * dp, numerosPaint)
            c.restore()
        }
    }

    /**
     * Elije el color de las lineas o de los numeros de acuerdo a la posicion
     * de las manecillas del selector.
     * */
    private fun elegirColor(paint: Paint, radianes: Double) {

        val margen = 0.005

        if (manecilla1.radianes < manecilla2.radianes) {

            paint.color = if (manecilla1.radianes - margen < radianes &&
                    manecilla2.radianes + margen> radianes) {
                BLACK
            } else {
                WHITE
            }

        } else {

            paint.color = if (manecilla1.radianes - margen > radianes &&
                    manecilla2.radianes + margen < radianes) {
                WHITE
            } else {
                BLACK
            }
        }
    }

    private fun dibujarArc(c: Canvas) {

        val startAngle = manecilla1.radianes.toAngle()
        var endAngle = manecilla2.radianes.toAngle() - startAngle

        endAngle = if (endAngle < 0) 360 + endAngle else endAngle

        c.drawArc(-radio, -radio, radio, radio,
                  startAngle, endAngle,
                  true,
                  paintArc)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (event.action == MotionEvent.ACTION_MOVE) {

            // Obtiene la distancia entre el toque y el centro de la vista
            val dx = event.x - (width / 2.0)
            val dy = event.y - (height / 2.0)

            // Obtiene los radianes de las coordenadas x e y
            var radianes = Math.atan2(dy, dx)

            // Convierte el rango de valores de -3.14, 3.14 a 0, 6.28
            radianes = if (radianes < 0.0) DOCE_HORAS + radianes else radianes

            // Convierte los radianes a un rango de progreso de 0.04363323129985823942309226921222
            // que equivales a un rango de progresos de 5 minutos
            val entre5Minutos = (radianes / CINCO_MINUTOS).toInt()
            val por5Minutos = entre5Minutos * CINCO_MINUTOS
            radianes = por5Minutos

            manecilla1.radianes = radianes
            manecilla2.radianes = radianes

            lanzarEventoTimeSelector()

            postInvalidateOnAnimation()
        }

        if (event.action == MotionEvent.ACTION_UP) {
            manecilla1.isTouch = false
            manecilla2.isTouch = false
            onScrollIsNoRunning = true
        }

        return gesture.onTouchEvent(event)
    }

    fun lanzarEventoTimeSelector() {

        val startTime = manecilla1.tiempo
        val endTime = manecilla2.tiempo

        if (startTime != oldStartTime || endTime != oldEndTime) {

            onTimeSelectorListener(startTime!!, endTime!!)

            oldStartTime = startTime
            oldEndTime = endTime
        }
    }

    private val simpleGesture = object : GestureDetector.SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent?): Boolean {
            return true
        }

        override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {

            if (onScrollIsNoRunning) {
                evaluarToque(e1!!)
                onScrollIsNoRunning = false
            }

            return true
        }

        override fun onLongPress(e: MotionEvent?) {
            evaluarToque(e!!)
        }

        override fun onSingleTapUp(e: MotionEvent?): Boolean {

            evaluarToque(e!!)

            manecilla1.cambiarMeridiano()
            manecilla2.cambiarMeridiano()

            lanzarEventoTimeSelector()

            manecilla1.isTouch = false
            manecilla2.isTouch = false

            parent.requestDisallowInterceptTouchEvent(true)

            postInvalidateOnAnimation()

            return true
        }

        private fun evaluarToque(e: MotionEvent) {

            val x = e.x - (width / 2f)
            val y = e.y - (height / 2f)

            manecilla1.isTouch(x, y)
            manecilla2.isTouch(x, y)

            if (manecilla2.isTouch) {

                manecilla1.isTouch = false
                parent.requestDisallowInterceptTouchEvent(true)

            } else if (manecilla1.isTouch) {

                manecilla2.isTouch = false
                parent.requestDisallowInterceptTouchEvent(true)

            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val modeW = View.MeasureSpec.getMode(widthMeasureSpec)
        val sizeW = View.MeasureSpec.getSize(widthMeasureSpec)
        val modeH = View.MeasureSpec.getMode(heightMeasureSpec)
        val sizeH = View.MeasureSpec.getSize(heightMeasureSpec)

        val minW = paddingStart + paddingEnd + (350 * dp).toInt()
        val minH = paddingTop + paddingBottom + (350 * dp).toInt()

        var w = minW
        var h = minH

        // Width
        when (modeW) {
            View.MeasureSpec.AT_MOST -> w = Math.min(sizeW, minW)
            View.MeasureSpec.EXACTLY -> w = Math.max(sizeW, minW)
            View.MeasureSpec.UNSPECIFIED -> w = minW
        }

        // Height
        when (modeH) {
            View.MeasureSpec.AT_MOST -> h = Math.min(sizeH, minH)
            View.MeasureSpec.EXACTLY -> h = Math.max(sizeH, minH)
            View.MeasureSpec.UNSPECIFIED -> h = minH
        }

        setMeasuredDimension(w, h)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)


        // Calcula el tamaño del radio del circulo basado en la altura
        // de la vista
        radio = (h * 0.75f) / 2

        manecilla1.radio = radio * 1.2f
        manecilla2.radio = radio * 1.2f
    }

    private fun Double.toAngle(): Float = (this * (180/Math.PI)).toFloat()

}
