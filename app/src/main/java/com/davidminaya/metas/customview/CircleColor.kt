package com.davidminaya.metas.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View

import com.davidminaya.metas.R

/**
 * Creada por david minaya el 28/05/2018 10:32.
 */
class CircleColor : View {

    private var fill: Paint? = null
    private var stroke: Paint? = null
    private var colorFill = Color.WHITE
    private var sizeStroke: Int = 0
    private var x: Int = 0
    private var y: Int = 0
    private var radio: Int = 0
    internal var select: Boolean = false

    var color: Int
        get() = colorFill
        set(color) {
            colorFill = color
            fill!!.color = color
            postInvalidate()
        }

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        if (attrs != null) {
            val atributos = context.theme.obtainStyledAttributes(attrs, R.styleable.CircleColor, 0, 0)
            try {
                colorFill = atributos.getColor(R.styleable.CircleColor_color_fill, Color.WHITE)
                select = atributos.getBoolean(R.styleable.CircleColor_select, false)
            } finally {
                atributos.recycle()
            }
        }

        fill = Paint(Paint.ANTI_ALIAS_FLAG)
        fill!!.style = Paint.Style.FILL
        fill!!.color = colorFill

        stroke = Paint(Paint.ANTI_ALIAS_FLAG)
        stroke!!.style = Paint.Style.STROKE
        stroke!!.color = Color.TRANSPARENT
    }

    fun setSelect(select: Boolean) {
        this.select = select
        invalidate()
    }


    override fun onDraw(canvas: Canvas) {

        if (select)
            stroke!!.color = ContextCompat.getColor(context, R.color.white_transparent)
        else
            stroke!!.color = Color.TRANSPARENT

        canvas.drawCircle(x.toFloat(), y.toFloat(), radio.toFloat(), stroke!!)
        canvas.drawCircle(x.toFloat(), y.toFloat(), radio.toFloat(), fill!!)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {

        x = w / 2
        y = h / 2

        // Calcula el ancho del borde del circulo
        sizeStroke = if (w < h)
            w / 5
        else
            h / 5

        // Calcula el radio del circulo
        radio = if (x < y)
            x - sizeStroke
        else
            y - sizeStroke

        stroke!!.strokeWidth = sizeStroke.toFloat()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        // Obtiene el modo del tamaño
        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)

        // Obtiene el tamaño de la vista
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        // Obtiene el tamaño minimo de la vista
        val widthMin = suggestedMinimumWidth + paddingStart + paddingEnd + 100
        val heightMin = suggestedMinimumHeight + paddingTop + paddingBottom + 100

        var width = 0
        var height = 0

        // Tamaño exacto
        if (widthMode == View.MeasureSpec.EXACTLY) width = widthSize
        if (heightMode == View.MeasureSpec.EXACTLY) height = heightSize

        // tan grande como su padre
        if (widthMode == View.MeasureSpec.AT_MOST) width = Math.min(widthMin, widthSize)
        if (heightMode == View.MeasureSpec.AT_MOST) height = Math.min(heightMin, heightSize)

        // Tan grande como su contenido
        if (widthMode == View.MeasureSpec.UNSPECIFIED) width = widthMin
        if (heightMode == View.MeasureSpec.UNSPECIFIED) height = heightMin

        // Ancho del borde
        sizeStroke = width / 10

        setMeasuredDimension(width, height)
    }
}
