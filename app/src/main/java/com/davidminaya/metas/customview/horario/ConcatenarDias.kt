package com.davidminaya.metas.customview.horario

import com.davidminaya.metas.database.entities.NombreDia

/**
 * Creada por david minaya el 26/10/2018 10:55 am.
 *
 * Concatena los días de la semana de acuerdo a la cantidad de días
 * seleccionados.
 *
 * Si hay menos de 4 días seleccionados estos se concatenan directamente. Por
 * ejemplo: Lunes, Martes y Miercoles.
 *
 * Si hay 4 o mas días seleccionados se elige uno de los grupos de días o se
 * concatenan los días abreviadamente. Existen 4 tipos de grupos que agrupan
 * una secuencia de 4, 5, 6 o 7 días. Cada tipo de grupo contiene 7 grupos que
 * abarcan todas las posibles combinaciones de secuencias que se pueden hacer
 * con una cantidad de días determinados. Por ejemplo, existen 7 grupos de 4
 * días. Cada grupo esta compuesto por una secuencia de días, la suma de esta
 * secuencia y el nombre del grupo. La secuencia esta compuesta por el número
 * de los dias; y el nombre del grupo es un resumen de la concatenacion de los
 * dias seleccionados, por ejemplo: "Lunes, Martes, Miercoles y Viernes" se
 * resume como "Lunes a Viernes".
 *
 * Dependiendo de la cantidad de días seleccionados se elige uno de los tipos
 * de los grupos de los dias. Si se seleccionan 4 días, se eligen los grupos de
 * 4 días, si uno de los grupos de 4 días coincide con los días seleccionados,
 * se elige ese grupo. De lo contrario se concatenan los días abreviadamente.
 *
 * Para que los días seleccionados coincidan con alguno de los grupos de los
 * días, estos deben tener la misma secuencia del grupo y la suma de los días
 * debe coincidir con la suma de la secuencia del grupo, si la secuencia y la
 * suma coinciden se selecciona el nombre del grupo. Por ejemplo: Los días
 * Lunes, Martes, Miercoles y Jueves forman la secuencia 1, 2, 3 y 4, y la suma
 * de esta secuencia es igual a 10, por lo que los días seleccionados coinciden
 * con el grupo 1 de los grupos de 4 dias, por lo que se elige el nombre de ese
 * grupo "Lunes a Jueves".
 *
 * Por el contrario si los días seleccionados no coinciden con ninguna de las
 * secuencias de los grupos (aunque la suma de estos días sea la misma que la
 * suma de la secuencia de uno de los grupos), se concatenan los días
 * abreviadamente. Por ejemplo: "Lunes, Miercoles, Viernes y Sabado" se
 * concatenan como "Lun, Mie, Vie y Sab". Esto se realiza de esta manera para
 * evita que la cadena resultante sea demaciado larga.
 */

private val grupoDe4Dias = arrayOf(
        GrupoDeDias(intArrayOf(1, 2, 3, 4), 10, "Lunes a Jueves"),
        GrupoDeDias(intArrayOf(2, 3, 4, 5), 14, "Martes a Viernes"),
        GrupoDeDias(intArrayOf(3, 4, 5, 6), 18, "Miercoles a Sabado"),
        GrupoDeDias(intArrayOf(4, 5, 6, 7), 22, "Jueves a Domingo"),
        GrupoDeDias(intArrayOf(5, 6, 7, 1), 19, "Viernes a Lunes"),
        GrupoDeDias(intArrayOf(6, 7, 1, 2), 16, "Sabado a Martes"),
        GrupoDeDias(intArrayOf(7, 1, 2, 3), 13, "Domingo a Miercoles")
)

private val grupoDe5Dias = arrayOf(
        GrupoDeDias(intArrayOf(1, 2, 3, 4, 5), 15, "Lunes a Viernes"),
        GrupoDeDias(intArrayOf(2, 3, 4, 5, 6), 20, "Martes a Sabado"),
        GrupoDeDias(intArrayOf(3, 4, 5, 6, 7), 25, "Miercoles a Domingo"),
        GrupoDeDias(intArrayOf(4, 5, 6, 7, 1), 23, "Jueves a Lunes"),
        GrupoDeDias(intArrayOf(5, 6, 7, 1, 2), 21, "Viernes a Martes"),
        GrupoDeDias(intArrayOf(6, 7, 1, 2, 3), 19, "Sabado a Miercoles"),
        GrupoDeDias(intArrayOf(7, 1, 2, 3, 4), 17, "Domingo a Jueves")
)

private val grupoDe6Dias = arrayOf(
        GrupoDeDias(intArrayOf(1, 2, 3, 4, 5, 6), 21, "Lunes a Sabado"),
        GrupoDeDias(intArrayOf(2, 3, 4, 5, 6, 7), 27, "Martes a Domingo"),
        GrupoDeDias(intArrayOf(3, 4, 5, 6, 7, 1), 26, "Miercoles a Lunes"),
        GrupoDeDias(intArrayOf(4, 5, 6, 7, 1, 2), 25, "Jueves a Martes"),
        GrupoDeDias(intArrayOf(5, 6, 7, 1, 2, 3), 24, "Viernes a Miercoles"),
        GrupoDeDias(intArrayOf(6, 7, 1, 2, 3, 4), 23, "Sabado a Jueves"),
        GrupoDeDias(intArrayOf(7, 1, 2, 3, 4, 5), 22, "Domingo a Viernes")
)

private val grupoDe7Dias = arrayOf(
        GrupoDeDias(intArrayOf(1, 2, 3, 4, 5, 6, 7), 28, "Toda la semana"),
        GrupoDeDias(intArrayOf(2, 3, 4, 5, 6, 7, 1), 28, "Toda la semana"),
        GrupoDeDias(intArrayOf(3, 4, 5, 6, 7, 1, 2), 28, "Toda la semana"),
        GrupoDeDias(intArrayOf(4, 5, 6, 7, 1, 2, 3), 28, "Toda la semana"),
        GrupoDeDias(intArrayOf(5, 6, 7, 1, 2, 3, 4), 28, "Toda la semana"),
        GrupoDeDias(intArrayOf(6, 7, 1, 2, 3, 4, 5), 28, "Toda la semana"),
        GrupoDeDias(intArrayOf(7, 1, 2, 3, 4, 5, 6), 28, "Toda la semana")
)


fun MutableList<NombreDia>.concatenarDias(): String {

    sortBy { it.nombreDiaId }

    return when {

        // Concatenar 1 día
        count() == 1 -> get(0).nombre!!

        // Concatenar 2 días
        count() == 2 -> "${get(0).nombre!!} y ${get(1).nombre!!}"

        // Concatenar 3 dias
        count() == 3 -> "${get(0).nombre!!}, ${get(1).nombre!!} y ${get(2).nombre!!}"

        // Concatenar 4 días
        count() == 4 -> evaluarGrupoDeDias(grupoDe4Dias)

        // Concatenar 5 días
        count() == 5 -> evaluarGrupoDeDias(grupoDe5Dias)

        // Concatenar 6 días
        count() == 6 -> evaluarGrupoDeDias(grupoDe6Dias)

        // Concatenar 7 días
        count() == 7 -> evaluarGrupoDeDias(grupoDe7Dias)

        else -> ""
    }
}


/*
* Evalua los dias seleccionados para saber si estos coinciden con alguno de los grupos
* de los dias
* */
private fun MutableList<NombreDia>.evaluarGrupoDeDias(grupoDeDias: Array<GrupoDeDias>): String {

    val diasSumados = sumBy { it.nombreDiaId!! }

    return when (diasSumados) {
        grupoDeDias[0].sumaDeLosDias -> concatenar(grupoDeDias[0])
        grupoDeDias[1].sumaDeLosDias -> concatenar(grupoDeDias[1])
        grupoDeDias[2].sumaDeLosDias -> concatenar(grupoDeDias[2])
        grupoDeDias[3].sumaDeLosDias -> concatenar(grupoDeDias[3])
        grupoDeDias[4].sumaDeLosDias -> concatenar(grupoDeDias[4])
        grupoDeDias[5].sumaDeLosDias -> concatenar(grupoDeDias[5])
        grupoDeDias[6].sumaDeLosDias -> concatenar(grupoDeDias[6])
        else -> concatenarDiasAbreviadamente()
    }
}

/*
 * Elige el nombre del grupo o concatena los dias abreviadamente
 */
private fun MutableList<NombreDia>.concatenar(grupoDeDias: GrupoDeDias): String {

    return if (esUnaSecuencia(grupoDeDias)) {
        grupoDeDias.nombre
    } else {
        concatenarDiasAbreviadamente()
    }
}

/*
 * Evalua los dias seleccionados para saber si son una secuencia
 */
private fun MutableList<NombreDia>.esUnaSecuencia(grupoDeDias: GrupoDeDias): Boolean {

    grupoDeDias.secuencia.sort()

    var esUnaSecuencia = true

    for (i in 0 until count()) {

        // Los dias de la semana estan numerados del 1 al 7. Si hay un día
        // que sumado mas uno es diferente al proximo día, entonces no hay
        // una secuencia de dias, si no dias al azar.
        if (get(i).nombreDiaId != grupoDeDias.secuencia[i]) esUnaSecuencia = false

    }

    return esUnaSecuencia
}

/*
 * Concatena los dias abreviadamente
 */
private fun MutableList<NombreDia>.concatenarDiasAbreviadamente(): String {

    // Abrevia el nombre de los dias
    val diasLista = map {
        NombreDia(it.nombre?.substring(0, 3)!!, it.nombreDiaId!!)
    }

    val penultimoDia = diasLista[diasLista.count() -2]
    val ultimoDia = diasLista.last()
    var dias = ""

    for (d in diasLista) {

        if (d != penultimoDia && d != ultimoDia) {
            dias += "${d.nombre}, "

        } else if (d == penultimoDia) {
            dias += "${d.nombre} y "

        } else if (d == ultimoDia) {
            dias += "${d.nombre}"
        }
    }

    return dias
}

private class GrupoDeDias(val secuencia: IntArray, val sumaDeLosDias: Int, val nombre: String)