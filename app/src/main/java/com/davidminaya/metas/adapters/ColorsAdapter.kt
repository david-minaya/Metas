package com.davidminaya.metas.adapters

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.davidminaya.metas.R

import com.davidminaya.metas.customview.CircleColor

/**
 * Creada por david minaya el 28/05/2018 12:40.
 */
class ColorsAdapter(c: Activity) : RecyclerView.Adapter<ColorsAdapter.ViewHolder>() {

    lateinit var onItemClick : (Int, Int) -> Unit
    private var lastPosition = 0
    private val colores: List<Int> = listOf(
        ContextCompat.getColor(c, R.color.blue),
        ContextCompat.getColor(c, R.color.red),
        ContextCompat.getColor(c, R.color.green),
        ContextCompat.getColor(c, R.color.orange),
        ContextCompat.getColor(c, R.color.purple)
    )

    private var circleColors = mutableListOf<CircleColor>()

    /**
     * Selecciona un item de la lista
     * @param position posición del item a seleccionar.
     */
    fun setLastPosition(position: Int) {
        lastPosition = position
        notifyItemChanged(position)
    }

    fun setSelectItemColor(color: Int) {
        val position = colores.indexOf(color)
        lastPosition = position
        notifyItemChanged(3)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val circleColor = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_colores, parent, false) as CircleColor

        // Almacena una referencia a todas las vistas creadas. Esta referencia se
        // utiliza más adelante para deseleccionar el último item seleccionado.
        circleColors.add(circleColor)

        return ViewHolder(circleColor, onItemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.circleColor.color = colores[position]
        selectDefaulItem(holder, position)
    }

    /**
     * Selecciona el item por defecto de la lista.
     *
     * Nota: este es el item que esta seleccionado al abrir la actividad o al rotar
     * el dispositivo.
     *
     * Este item solo se selecciona al crear el recyclerView o al llamar el método
     * setLastPosition() (este método se llama al restaurar la actividad). En ambos
     * casos se selecciona el item que tiene la misma posición que el último item
     * seleccionado (la posición del último item seleccionado se almacena en la
     * variable lastPosition). En el primer caso se selecciona el item que esta en la
     * posicion cero de la lista, ya que el valor inicial de lastPosition es cero. En
     * el segundo caso el método setLastPosition() se utiliza para establecer el valor
     * de la última posicion seleccionada. Cuando este método se llama estable el
     * valor de la variable lastPosition y llama el método notifyItemChanged(), al que
     * se le pasa como argumento el valor de la última posición seleccionada. El
     * método notifyItemChanged() llama al método onBindViewHolder() y le pasa como
     * argumento la última posición seleccionada, como resultado el parametro position
     * del método onBindViewHolder() y el valor de la variable lastPosition son
     * iguales, por lo que se selecciona el item que se encuentra en esa posición.
     *
     * @param holder item a seleccionar
     * @param position parametro position del metodo onBindViewHolder()
     */
    private fun selectDefaulItem(holder: ViewHolder, position: Int) {
        if (lastPosition == position) {
            holder.circleColor.setSelect(true)
            onItemClick(colores[position], position)
        }
    }

    override fun getItemCount(): Int {
        return colores.size
    }

    inner class ViewHolder(var circleColor: CircleColor) : RecyclerView.ViewHolder(circleColor) {

        lateinit var onItemClick: (Int, Int) -> Unit

        constructor(circleColor: CircleColor, onItemClick: (Int, Int) -> Unit): this(circleColor) {
            this.onItemClick = onItemClick
            circleColor.setOnClickListener(this::onClick)
        }

        private fun onClick(v: View) {

            val circleColor = v as CircleColor

            // Selecciona el item actualmente seleccionado
            circleColor.setSelect(true)

            // Deselecciona el ultimo item seleccionado
            circleColors[lastPosition].setSelect(false)

            onItemClick(circleColor.color, adapterPosition)

            /* Almacena la posición del item seleccionado. La posicion del item seleccionado
             * sera la posicion del ultimo item seleccionado en la proxima seleccion.
             * Es importante almacenar la posición del item seleccionado despues de deseleccionar
             * el último item seleccionado. De lo contrario se deseleccionaria el item actual.
             */
            lastPosition = adapterPosition
        }
    }
}
