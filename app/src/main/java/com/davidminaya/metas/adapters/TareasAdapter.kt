package com.davidminaya.metas.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.davidminaya.metas.R
import com.davidminaya.metas.adapters.TareasAdapter.ViewHolder
import com.davidminaya.metas.customview.ProgressBar
import com.davidminaya.metas.database.entities.Tarea
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.ChronoUnit

class TareasAdapter(private val context: Context,
                    private val tareas: List<Tarea>,
                    private val onClickItemListener: (Tarea) -> Unit): RecyclerView.Adapter<ViewHolder>() {

    private val r: Resources = context.resources

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_tareas, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = tareas.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val fechaDeInicio = LocalDate.parse(tareas[position].fechaDeInicio)
        val fechaDeFinalizacion = LocalDate.parse(tareas[position].fechaDeFinalizacion)

        val unidadDeProgreso = 100f / 365
        val dias = ChronoUnit.DAYS.between(fechaDeInicio, fechaDeFinalizacion)
        val progreso = (dias * unidadDeProgreso).toInt()

        holder.nombre.text = tareas[position].nombre

        holder.progreso.setColorProgress(tareas[position].color)
        holder.progreso.progress = progreso
        holder.progreso.isTaskFinish = tareas[position].isFinalizada

        holder.fechaDeInicio.text = "${r.getString(R.string.desde_el)} ${formatearFecha(fechaDeInicio)}"
        holder.fechaDeFinalizacion.text = "${r.getString(R.string.hasta_el)} ${formatearFecha(fechaDeFinalizacion)}"

    }

    private fun formatearFecha(fecha: LocalDate): String {

        val formatter = DateTimeFormatter.ofPattern("MMM")
        val mes = fecha.format(formatter).decapitalize().substringAfter(";")

        return "${fecha.dayOfMonth} ${r.getString(R.string.de)} $mes ${fecha.year}"
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val nombre: TextView = view.findViewById(R.id.nombre)
        val progreso: ProgressBar = view.findViewById(R.id.progreso)
        val fechaDeInicio: TextView = view.findViewById(R.id.fechaDeInicio)
        val fechaDeFinalizacion: TextView = view.findViewById(R.id.fechaDeFinalizacion)

        init {

            itemView.setOnClickListener {
                onClickItemListener(tareas[adapterPosition])
            }
        }
    }
}