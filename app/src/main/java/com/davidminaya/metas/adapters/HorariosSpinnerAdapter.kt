package com.davidminaya.metas.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.davidminaya.metas.database.entities.Horario

/**
 * Creada por david minaya el 19/09/2018 12:24.
 *
 */
class HorariosSpinnerAdapter(val context: Context) : BaseAdapter() {

    var horarios: MutableList<Horario> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)

        val textView = view.findViewById<TextView>(android.R.id.text1)
        textView.text = horarios[position].nombre

        return view
    }

    override fun getItem(position: Int): Horario {
        return horarios[position]
    }

    fun getPosition(horarioId: Int?): Int {

        if (horarioId == null) return 0

        return horarios.indexOfFirst { horarioId == it.horarioId }
    }

    override fun getItemId(position: Int): Long {
        return 0L
    }

    override fun getCount(): Int {
        return horarios.size
    }

}