package com.davidminaya.metas.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.davidminaya.metas.R
import com.davidminaya.metas.adapters.DiasAdapter.DiasViewHolder
import com.davidminaya.metas.database.entities.NombreDia

/**
 * Creada por david minaya el 01/10/2018 12:47.
 *
 */
class DiasAdapter(val c: Context, val diasClickListener: (NombreDia) -> Unit): RecyclerView.Adapter<DiasViewHolder>() {

    var dias = listOf<NombreDia>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiasViewHolder {
        val item = LayoutInflater.from(c).inflate(R.layout.item_dias, parent, false) as TextView
        return DiasViewHolder(item)
    }

    override fun getItemCount(): Int {
        return dias.size
    }

    override fun onBindViewHolder(holder: DiasViewHolder, position: Int) {
        holder.item.text = dias[position].nombre?.substring(0, 1) ?: ""
        holder.selectItem()
    }

    inner class DiasViewHolder(val item: TextView): ViewHolder(item) {

        init {
            item.setOnClickListener(this::onClick)
        }

        private fun onClick(v: View) {

            if (!dias[adapterPosition].isSelected) {

                item.setTextColor(ContextCompat.getColor(c, R.color.black_dark))
                item.setBackgroundResource(R.drawable.selected_day)
                dias[adapterPosition].isSelected = true

            } else {

                item.setTextColor(ContextCompat.getColor(c, R.color.white))
                item.setBackgroundResource(R.drawable.unselected_day)
                dias[adapterPosition].isSelected = false
            }

            diasClickListener(dias[adapterPosition])
        }

        /**
         * Selecciona los item que se habian seleccionado antes de destruir la
         * Actividad.
         *
         * La lista de los dias que se muestran en el RecyclerView se obtienen
         * de un View Model. Los objetos creados en el View Model sobreviven a
         * ciclo de vida de la Actividad. Por lo que al marcar un dia oomo
         * seleccionado este se mantiene seleccionado aunque la actividad se
         * destruya. Por lo que para volver a seleccionar los items que se
         * habian seleccionado antes de destruir la actividad, solo hay que leer
         * la propiedad isSelected de los dias, si es true se selecciona el
         * item, si es false no se selecciona.
         * */
        fun selectItem() {

            if (dias[adapterPosition].isSelected) {

                item.setTextColor(ContextCompat.getColor(c, R.color.black_dark))
                item.setBackgroundResource(R.drawable.selected_day)

            } else {

                item.setTextColor(ContextCompat.getColor(c, R.color.white))
                item.setBackgroundResource(R.drawable.unselected_day)
            }

            diasClickListener(dias[adapterPosition])
        }
    }
}