package com.davidminaya.metas.servicios

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.LocalBroadcastManager
import com.davidminaya.metas.R
import com.davidminaya.metas.actividades.MainActivity
import com.davidminaya.metas.receivers.TareasReceiver
import com.davidminaya.metas.database.MetasDB
import com.davidminaya.metas.database.entities.Tarea
import com.davidminaya.metas.utils.*
import com.davidminaya.metas.utils.ParcelableUtil
import org.threeten.bp.*
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.ChronoField
import org.threeten.bp.temporal.ChronoUnit


class EjecutarTareaService : Service() {

    companion object {
        var isRunning = false
    }

    private lateinit var db: MetasDB
    lateinit var tarea: Tarea
    private lateinit var thread: HandlerThread
    private lateinit var handler: Handler
    private lateinit var receiver: Receiver
    private lateinit var broadcastManager: LocalBroadcastManager
    private lateinit var notificacion: NotificationCompat.Builder
    private lateinit var horaDeInicio: LocalTime
    private lateinit var horaDeFinalizacion: LocalTime
    private var delayed = 0L

    var progress = 0
        private set

    override fun onCreate() {
        super.onCreate()

        db = MetasDB.getMetasDB(this)

        isRunning = true
        thread = HandlerThread("EjecutarTareaServiceThread")
        thread.start()
        handler = Handler(thread.looper)

        broadcastManager = LocalBroadcastManager.getInstance(this)

        receiver = Receiver()
        val filters = IntentFilter().apply {
            addAction(Intent.ACTION_TIME_CHANGED)
            addAction(Intent.ACTION_TIMEZONE_CHANGED)
            addAction(ACTION_CANCELAR_TAREA)
        }

        registerReceiver(receiver, filters)
        //registerReceiver(NotificationReceiver(), IntentFilter(ACTION_CANCELAR_TAREA))
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        tarea = intent?.getParcelableExtra(EXTRA_TAREA)!!

        val intentTarea = Intent(FILTER_TAREA_START)
        intentTarea.putExtra(EXTRA_TAREA, tarea)
        broadcastManager.sendBroadcast(intentTarea)

        lanzarNotificacion()

        handler.post{

            val horario = db.horarioDao().horarioSync(tarea.horarioId!!)

            horaDeInicio = LocalTime.parse(horario.horaDeInicio)
            horaDeFinalizacion = LocalTime.parse(horario.horaDeFinalizacion)

            ejecutarProgressRunnable()
            programarProximaEjecucion()
        }

        return START_STICKY
    }

    private fun lanzarNotificacion() {

        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val intentCancelar = Intent(ACTION_CANCELAR_TAREA)
        val pendingCancelar = PendingIntent.getBroadcast(this, 1, intentCancelar, PendingIntent.FLAG_UPDATE_CURRENT)

        notificacion = NotificationCompat.Builder(this, "com.davidminaya.metas")
        notificacion.setContentTitle(tarea.nombre)
        notificacion.setSmallIcon(R.drawable.icono_notificacion)
        notificacion.priority = NotificationCompat.PRIORITY_DEFAULT
        notificacion.setContentIntent(pendingIntent)
        notificacion.color = tarea.color
        notificacion.addAction(R.drawable.icono_notificacion, getString(R.string.cancelar), pendingCancelar)

        if (!tarea.descripcion.isNullOrBlank()) {
            notificacion.setContentText(tarea.descripcion)
        }

        NotificationManagerCompat.from(this).notify(1, notificacion.build())
    }

    override fun onBind(intent: Intent): IBinder = LocalBinder()

    inner class LocalBinder: Binder() {
        fun getService(): EjecutarTareaService = this@EjecutarTareaService
    }

    private fun ejecutarProgressRunnable() {

        val milisegundos = ChronoUnit.MILLIS.between(horaDeInicio, horaDeFinalizacion)
        delayed = milisegundos / 100L
        handler.removeCallbacks(progressRunnable)
        handler.post(progressRunnable)
    }

    private fun programarProximaEjecucion() {

        val nDiaActual = LocalDate.now().dayOfWeek.value.toLong()

        val fechaDeEjecucion = LocalDateTime.now()
                .plusWeeks(1)
                .with(ChronoField.DAY_OF_WEEK, nDiaActual)
                .withHour(horaDeInicio.hour)
                .withMinute(horaDeInicio.minute)
                .withSecond(0)
                .toInstant(OffsetDateTime.now().offset) // Obtiene la zona horaria actual
                .toEpochMilli()

        val intent = Intent(this, TareasReceiver::class.java)
        intent.action = ACTION_EJECUTAR_TAREA
        intent.putExtra(EXTRA_TAREA, ParcelableUtil.marshall(tarea))

        val pendingIntentId = (tarea.hashCode() * nDiaActual).toInt()
        val pendingIntent = PendingIntent.getBroadcast(this, pendingIntentId, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, fechaDeEjecucion, pendingIntent)
        } else {
            alarmManager.setExact(AlarmManager.RTC, fechaDeEjecucion, pendingIntent)
        }
    }

    fun cancelarTarea() {

        // Notifica al fragmento MainFragment que la ejecucion de la tarea ha finalizado
        broadcastManager.sendBroadcast(Intent(FILTER_TAREA_FINISH))

        NotificationManagerCompat.from(this).cancel(1)

        // Detiene la tarea y el servicio
        handler.removeCallbacks(progressRunnable)
        stopSelf()
    }

    private val progressRunnable = object: Runnable {

        override fun run() {

            handler.postDelayed(this, delayed)

            // Calcula el progreso de la tarea
            val horaActual = LocalTime.now()
            val milisegundos = ChronoUnit.MILLIS.between(horaDeInicio, horaDeFinalizacion)
            val milisegundo2 = ChronoUnit.MILLIS.between(horaDeInicio, horaActual)
            progress = ((milisegundo2.toFloat() / milisegundos.toFloat()) * 100).toInt()

            // Envia el progreso al fragmento MainFragment
            val intent = Intent(FILTER_TAREA_PROGRESS)
            intent.putExtra(EXTRA_PROGRESS, progress)
            broadcastManager.sendBroadcast(intent)

            // Si el progreso es mayor o igual a 100 termina la ejecucion de la tarea.
            // O Si el progreso de la tarea es menor a 0 detiene el servicio. Este caso
            // se puede dar cuando el ususario cambia la hora del dispositivo a una
            // hora menor despues de iniciar la tarea.
            if (progress < 0 || progress >= 100) {

                if (progress >= 100) {

                    val fechaActual = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)
                    db.tareaDao().actualizarFechaDeFinalizacion(fechaActual, tarea.tareaId!!)

                    NotificationManagerCompat.from(this@EjecutarTareaService).cancel(1)

                    broadcastManager.sendBroadcast(Intent(FILTER_TAREA_DONE))

                    // Detiene la tarea y el servicio
                    handler.removeCallbacks(this)
                    stopSelf()

                } else {

                    cancelarTarea()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        isRunning = false
        thread.quit()
        unregisterReceiver(receiver)

    }

    inner class Receiver: BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {

            when (intent?.action) {

                Intent.ACTION_TIME_CHANGED , Intent.ACTION_TIMEZONE_CHANGED -> {
                    ejecutarProgressRunnable()
                }

                ACTION_CANCELAR_TAREA -> {
                    cancelarTarea()
                }
            }
        }
    }
}
