package com.davidminaya.metas.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

import com.davidminaya.metas.database.entities.NombreDia

/**
 * Creada por david minaya el 09/08/2018 5:49.
 */
@Dao
interface NombreDiaDao {

    @Query("SELECT * FROM nombre_dias")
    fun dias(): LiveData<List<NombreDia>>

    @Insert
    fun insert(vararg nombreDias: NombreDia)

    @Update
    fun update(vararg nombreDias: NombreDia)

    @Delete
    fun delete(vararg nombreDias: NombreDia)

    @Query("DELETE FROM nombre_dias")
    fun deleteAll()
}
