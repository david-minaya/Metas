package com.davidminaya.metas.database.entities

import android.arch.persistence.room.Relation

class HorarioDias {

    var horarioId: Int? = null
    var horaDeInicio: String? = null
    var horaDeFinalizacion: String? = null

    @Relation(entity = Dia::class, parentColumn = "horarioId", entityColumn = "horarioId")
    var dias: List<Dia>? = null
}