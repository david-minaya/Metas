package com.davidminaya.metas.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.davidminaya.metas.database.entities.*

/**
 * Creada por david minaya el 09/08/2018 5:41.
 */
@Dao
interface HorarioDao {

    @Query("SELECT * FROM horarios WHERE horarioId = :id")
    fun horario(id: Int): LiveData<Horario>

    @Query("SELECT * FROM horarios WHERE horarioId = :id")
    fun horarioSync(id: Int): Horario

    @Query("SELECT horarioId, horaDeInicio, horaDeFinalizacion FROM horarios WHERE horarioId = :id")
    fun horarioDias(id: Int): HorarioDias

    @Query("SELECT horarioId, horaDeInicio, horaDeFinalizacion FROM horarios ORDER BY horaDeInicio")
    fun horariosDias(): List<HorarioDias>

    @Query("SELECT * FROM horarios WHERE horarioId = :id")
    fun horarioTareasDias(id: Int): HorarioTareasDias

    @Query("SELECT * FROM horarios")
    fun horarios(): LiveData<MutableList<Horario>>

    @get:Query("SELECT * FROM horarios ORDER BY horaDeInicio")
    @get:Transaction
    val horariosTareasDias: List<HorarioTareasDias>

    @Query("SELECT horarios.*, tareas.tareaId AS tarea_tareaId, tareas.nombre AS tarea_nombre," +
            "tareas.fechaDeInicio AS tarea_fechaDeInicio, tareas.fechaDeFinalizacion AS tarea_fechaDeFinalizacion," +
            "tareas.descripcion AS tarea_descripcion, tareas.isFinalizada AS tarea_isFinalizada," +
            "tareas.horarioId AS tarea_horarioId, tareas.color AS tarea_color " +
           "FROM horarios " +
           "JOIN tareas ON horarios.horarioId = tareas.horarioId " +
           "GROUP BY horarios.horarioId")
    fun horariosTareaDias(): List<HorarioTareaDias>

    @Insert
    fun insert(horarios: Horario): Long

    @Insert
    fun insert(vararg horarios: Horario)

    @Query("UPDATE horarios " +
           "SET nombre = :nombre, " +
               "horaDeInicio = :horaDeInicio, " +
               "horaDeFinalizacion = :horaDeFinalizacion " +
           "WHERE horarioId = :id")
    fun update(nombre: String, horaDeInicio: String, horaDeFinalizacion: String, id: Int)

    @Delete
    fun delete(vararg horarios: Horario)

    @Query("DELETE FROM horarios")
    fun deleteAll()
}
