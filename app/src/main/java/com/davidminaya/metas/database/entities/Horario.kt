package com.davidminaya.metas.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

/**
 * Creada por david minaya el 09/08/2018 5:09.
 */
@Entity(tableName = "horarios")
class Horario() : Parcelable {

    @PrimaryKey
    var horarioId: Int? = null
    var nombre: String? = null
    var horaDeInicio: String? = null
    var horaDeFinalizacion: String? = null

    constructor(parcel: Parcel) : this() {
        horarioId = parcel.readValue(Int::class.java.classLoader) as? Int
        nombre = parcel.readString()
        horaDeInicio = parcel.readString()
        horaDeFinalizacion = parcel.readString()
    }

    constructor(nombre: String, horaDeInicio: String, horaDeFinalizacion: String): this() {
        this.nombre = nombre
        this.horaDeInicio = horaDeInicio
        this.horaDeFinalizacion = horaDeFinalizacion
    }

    constructor(id: Int, nombre: String, horaDeInicio: String, horaDeFinalizacion: String)
            :this(nombre, horaDeInicio, horaDeFinalizacion)
    {
        horarioId = id
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(horarioId)
        parcel.writeString(nombre)
        parcel.writeString(horaDeInicio)
        parcel.writeString(horaDeFinalizacion)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Horario> {
        override fun createFromParcel(parcel: Parcel): Horario {
            return Horario(parcel)
        }

        override fun newArray(size: Int): Array<Horario?> {
            return arrayOfNulls(size)
        }
    }
}
