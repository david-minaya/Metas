package com.davidminaya.metas.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

/**
 * Creada por david minaya el 09/08/2018 5:10.
 */
@Entity(tableName = "dias", indices = [Index("horarioId"), Index("nombreDiaId")]
)
class Dia() : Parcelable {

    @PrimaryKey
    var diaId: Int?= null
    var horarioId: Int? = null
    var nombreDiaId: Int? = null

    constructor(parcel: Parcel) : this() {
        diaId = parcel.readValue(Int::class.java.classLoader) as? Int
        horarioId = parcel.readInt()
        nombreDiaId = parcel.readInt()
    }

    constructor(horarioId: Int, nombreDiaId: Int): this() {
        this.horarioId = horarioId
        this.nombreDiaId = nombreDiaId
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(diaId)
        parcel.writeInt(horarioId!!)
        parcel.writeInt(nombreDiaId!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Dia> {
        override fun createFromParcel(parcel: Parcel): Dia {
            return Dia(parcel)
        }

        override fun newArray(size: Int): Array<Dia?> {
            return arrayOfNulls(size)
        }
    }
}
