package com.davidminaya.metas.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

import com.davidminaya.metas.database.entities.Dia

/**
 * Creada por david minaya el 09/08/2018 5:46.
 */
@Dao
interface DiaDao {

    @Query("SELECT * FROM dias")
    fun select(): LiveData<List<Dia>>

    @Query("SELECT nombreDiaId FROM dias WHERE horarioId = :horarioId")
    fun dias(horarioId: Int): List<Int>

    @Insert
    fun insert(vararg dias: Dia)

    @Delete
    fun delete(vararg dia: Dia)

    @Query("DELETE FROM dias")
    fun deleteAll()

    @Query("DELETE FROM dias WHERE horarioId = :id")
    fun borrarDiasDelHorario(id: Int)
}
