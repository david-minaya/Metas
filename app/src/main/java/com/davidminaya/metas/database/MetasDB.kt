package com.davidminaya.metas.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.*
import android.arch.persistence.room.migration.Migration
import android.content.Context
import android.os.AsyncTask

import com.davidminaya.metas.database.dao.DiaDao
import com.davidminaya.metas.database.dao.HorarioDao
import com.davidminaya.metas.database.dao.NombreDiaDao
import com.davidminaya.metas.database.dao.TareaDao
import com.davidminaya.metas.database.entities.Dia
import com.davidminaya.metas.database.entities.Horario
import com.davidminaya.metas.database.entities.NombreDia
import com.davidminaya.metas.database.entities.Tarea

/**
 * Creada por david minaya el 09/08/2018 4:55.
 */
@Database(entities = [Horario::class, Tarea::class, Dia::class, NombreDia::class], version = 3)
abstract class MetasDB : RoomDatabase() {

    abstract fun horarioDao(): HorarioDao
    abstract fun tareaDao(): TareaDao
    abstract fun diasDao(): DiaDao
    abstract fun nombreDiaDao(): NombreDiaDao

    companion object {

        private lateinit var metasDB: MetasDB

        fun getMetasDB(context: Context): MetasDB {

            synchronized(MetasDB::class) {
                metasDB = Room.databaseBuilder(
                                context.applicationContext,
                                MetasDB::class.java,
                                "metas.db")
                        .addCallback(fillDatabase)
                        .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                        .build()
            }

            return metasDB
        }

        private val MIGRATION_1_2 = object: Migration(1, 2) {

            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("DROP TABLE dias;")
                database.execSQL("CREATE TABLE dias (" +
                        "diaId INTEGER PRIMARY KEY," +
                        "horarioId INTEGER," +
                        "nombreDiaId INTEGER," +
                        "FOREIGN KEY(horarioId) REFERENCES horarios(horarioId) ON UPDATE CASCADE ON DELETE CASCADE," +
                        "FOREIGN KEY(nombreDiaId) REFERENCES nombre_dias(nombreDiaId) ON UPDATE CASCADE ON DELETE CASCADE);")
                database.execSQL("CREATE INDEX index_dias_horarioId ON dias(horarioId);")
                database.execSQL("CREATE INDEX index_dias_nombreDiaId ON dias(nombreDiaId);")
            }
        }

        private val MIGRATION_2_3 = object: Migration(2, 3) {

            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("DROP TABLE dias;")
                database.execSQL("CREATE TABLE dias (" +
                        "diaId INTEGER PRIMARY KEY," +
                        "horarioId INTEGER," +
                        "nombreDiaId INTEGER);")
                database.execSQL("CREATE INDEX index_dias_horarioId ON dias(horarioId);")
                database.execSQL("CREATE INDEX index_dias_nombreDiaId ON dias(nombreDiaId);")
            }
        }

        private val fillDatabase = object: RoomDatabase.Callback() {

            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                AsyncTaskFillDatabase(metasDB).execute()
            }
        }

        private class AsyncTaskFillDatabase() : AsyncTask<Void, Void, Unit>() {

            private lateinit var db: MetasDB

            constructor(db: MetasDB) : this() {
                this.db = db
            }

            override fun doInBackground(vararg params: Void?) {

                val nombreDias = arrayOf(
                        NombreDia("Lunes"),
                        NombreDia("Martes"),
                        NombreDia("Miercoles"),
                        NombreDia("Jueves"),
                        NombreDia("Viernes"),
                        NombreDia("Sabado"),
                        NombreDia("Domingo")
                )

                // Borra la base de datos
                db.run {
                    nombreDiaDao().deleteAll()
                    nombreDiaDao().insert(*nombreDias)
                }
            }
        }
    }
}
