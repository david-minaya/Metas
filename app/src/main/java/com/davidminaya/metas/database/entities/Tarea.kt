package com.davidminaya.metas.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

/**
 * Creada por david minaya el 09/08/2018 5:10.
 */
@Entity(tableName = "tareas",
        indices = [Index("horarioId"), Index("color")],
        foreignKeys = [
                ForeignKey(
                entity = Horario::class,
                parentColumns = arrayOf("horarioId"),
                childColumns = arrayOf("horarioId"),
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)
        ]
)
class Tarea() : Parcelable {

    @PrimaryKey
    var tareaId: Int? = null
    var nombre = ""
    var fechaDeInicio: String? = null
    var fechaDeFinalizacion: String? = null
    var duracion = 0
    var descripcion: String? = null
    var isFinalizada = false
    var horarioId: Int? = null
    var color = 0

    constructor(parcel: Parcel) : this() {
        tareaId = parcel.readValue(Int::class.java.classLoader) as? Int
        nombre = parcel.readString()
        fechaDeInicio = parcel.readString()
        fechaDeFinalizacion = parcel.readString()
        duracion = parcel.readInt()
        descripcion = parcel.readString()
        isFinalizada = parcel.readByte() != 0.toByte()
        horarioId = parcel.readValue(Int::class.java.classLoader) as? Int
        color = parcel.readInt()
    }

    constructor(nombre: String, horarioId: Int, color: Int, fechaDeInicio: String?, fechaDeFinalizacion: String?, duracion: Int, descripcion: String?, isFinalizada: Boolean):this() {
        this.nombre = nombre
        this.fechaDeInicio = fechaDeInicio
        this.fechaDeFinalizacion = fechaDeFinalizacion
        this.duracion = duracion
        this.descripcion = descripcion
        this.isFinalizada = isFinalizada
        this.horarioId = horarioId
        this.color = color
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(tareaId)
        parcel.writeString(nombre)
        parcel.writeString(fechaDeInicio)
        parcel.writeString(fechaDeFinalizacion)
        parcel.writeInt(duracion)
        parcel.writeString(descripcion)
        parcel.writeByte(if (isFinalizada) 1 else 0)
        parcel.writeValue(horarioId)
        parcel.writeInt(color)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Tarea> {
        override fun createFromParcel(parcel: Parcel): Tarea {
            return Tarea(parcel)
        }

        override fun newArray(size: Int): Array<Tarea?> {
            return arrayOfNulls(size)
        }
    }
}
