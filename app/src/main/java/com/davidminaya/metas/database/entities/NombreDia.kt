package com.davidminaya.metas.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

/**
 * Creada por david minaya el 09/08/2018 5:10.
 */
@Entity(tableName = "nombre_dias")
class NombreDia {

    @PrimaryKey var nombreDiaId: Int? = null
    var nombre: String? = null
    @Ignore var isSelected = false

    constructor(nombre: String) {
        this.nombre = nombre
    }

    constructor(nombre: String, id: Int): this(nombre) {
        nombreDiaId = id
    }
}
