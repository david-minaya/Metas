package com.davidminaya.metas.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

import com.davidminaya.metas.database.entities.Tarea

/**
 * Creada por david minaya el 09/08/2018 5:44.
 */
@Dao
interface TareaDao {

    @Query("SELECT * FROM tareas")
    fun select(): LiveData<List<Tarea>>

    @Query("SELECT * FROM tareas")
    fun tareas(): List<Tarea>

    @Query("SELECT * FROM tareas WHERE horarioId = :horarioId AND isFinalizada = :isFinish")
    fun existeAlgunaTareaEnEjecucion(horarioId: Int, isFinish: Boolean = false): List<Tarea>

    @Query("SELECT * FROM tareas WHERE tareaId = :id")
    fun tarea(id: Int): Tarea

    @Query("SELECT * FROM tareas WHERE horarioId = :horarioId ORDER BY tareaId")
    fun ultimaTarea(horarioId: Int): Tarea

    @Insert
    fun insert(vararg tareas: Tarea)

    @Insert
    fun insertarTarea(tarea: Tarea): Long

    @Update
    fun update(vararg tareas: Tarea)

    @Query("UPDATE tareas SET fechaDeFinalizacion = :fecha WHERE tareaId = :id")
    fun actualizarFechaDeFinalizacion(fecha: String, id: Int)

    @Delete
    fun delete(vararg tareas: Tarea)

    @Query("DELETE FROM tareas")
    fun deleteAll()
}
