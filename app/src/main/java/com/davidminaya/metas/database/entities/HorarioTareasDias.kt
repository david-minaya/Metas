package com.davidminaya.metas.database.entities

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import android.os.Parcel
import android.os.Parcelable

class HorarioTareasDias() : Parcelable {

    @Embedded
    var horario: Horario? = null

    @Relation(entity = Tarea::class, parentColumn = "horarioId", entityColumn = "horarioId")
    var tareas: List<Tarea>? = null

    @Relation(entity = Dia::class, parentColumn = "horarioId", entityColumn = "horarioId")
    var dias: List<Dia>? = null

    constructor(parcel: Parcel) : this() {
        horario = parcel.readParcelable(Horario::class.java.classLoader)
        tareas = parcel.createTypedArrayList(Tarea)
        dias = parcel.createTypedArrayList(Dia)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(horario, flags)
        parcel.writeTypedList(tareas)
        parcel.writeTypedList(dias)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HorarioTareasDias> {
        override fun createFromParcel(parcel: Parcel): HorarioTareasDias {
            return HorarioTareasDias(parcel)
        }

        override fun newArray(size: Int): Array<HorarioTareasDias?> {
            return arrayOfNulls(size)
        }
    }
}