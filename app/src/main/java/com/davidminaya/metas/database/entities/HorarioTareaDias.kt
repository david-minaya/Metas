package com.davidminaya.metas.database.entities

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import android.os.Parcel
import android.os.Parcelable

/**
 * Creada por David Minaya el 08/11/2018 a las 1:18 pm
 * */
class HorarioTareaDias(): Parcelable {

    @Embedded
    var horario: Horario? = null

    @Embedded(prefix = "tarea_")
    var tarea: Tarea? = null

    @Relation(entity = Dia::class, parentColumn = "horarioId", entityColumn = "horarioId")
    var dias: List<Dia>? = null

    constructor(parcel: Parcel) : this() {
        horario = parcel.readParcelable(Horario::class.java.classLoader)
        tarea = parcel.readParcelable(Tarea::class.java.classLoader)
        dias = parcel.createTypedArrayList(Dia)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(horario, flags)
        parcel.writeParcelable(tarea, flags)
        parcel.writeTypedList(dias)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HorarioTareaDias> {
        override fun createFromParcel(parcel: Parcel): HorarioTareaDias {
            return HorarioTareaDias(parcel)
        }

        override fun newArray(size: Int): Array<HorarioTareaDias?> {
            return arrayOfNulls(size)
        }
    }


}