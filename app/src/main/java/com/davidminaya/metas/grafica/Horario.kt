package com.davidminaya.metas.grafica

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.*
import android.graphics.Color.WHITE
import com.davidminaya.metas.database.entities.HorarioTareasDias

import com.davidminaya.metas.utils.transparentar

import java.util.ArrayList

/**
 * Creada por david minaya el 19/07/2018 11:39.
 */
class Horario(val horarioTareasDias: HorarioTareasDias, y: Float) {

    companion object {

        var dp: Float = 0f
        var sp: Float = 0f
        var widthView: Int = 0

        fun setDisplayMetrics(dp: Float, sp: Float) {
            this.dp = dp
            this.sp = sp
        }
    }

    private var titulo: String? = null
    internal var y = 0f
    var tareas: MutableList<Tarea>
    private val paint = Paint()

    /**
     * Cuadro delimitador del horario
     *
     * ```
     * +-----------------------------------+
     * |  Lunes a viernes de 6 am a 10 pm  |
     * |                                   |
     * |   +---------------------------+   |
     * |   |          Tarea            |   |
     * |   | (///////////////////////) |   |
     * |   +---------------------------+   |
     * +-----------------------------------+
     * ```
     */
    var bound: RectF
        private set

    private var paintRipple: Paint
    private var animator: ValueAnimator
    private var xTouch = 0f
    private var yTouch = 0f
    private var radio = 0f
    private lateinit var horarioListener: (HorarioTareasDias) -> Unit
    private var isTouch = false
        set(value) {
            field = value
            if (value) {
                animator.start()
            }
        }
    var rippleAnimationIsRunning = false
        get() = animator.isRunning

    init {

        this.titulo = horarioTareasDias.horario?.nombre
        this.y = y * dp

        tareas = ArrayList()
        for (tarea in horarioTareasDias.tareas!!) {
            tareas.add(Tarea(tarea))
        }

        bound = RectF()
        bound.left = 0f
        bound.right = widthView.toFloat()
        bound.top = this.y - 20 * dp
        bound.bottom = this.y + 64 * dp

        paint.isAntiAlias = true
        paint.textSize = 14 * sp
        paint.textAlign = Paint.Align.CENTER
        paint.color = transparentar(WHITE, 75)
        paint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)

        paintRipple = Paint()
        paintRipple.isAntiAlias = true
        paintRipple.color = transparentar(WHITE, 25)

        animator = ObjectAnimator.ofFloat(8 * dp, widthView.toFloat())
        animator.duration = 350
        animator.addUpdateListener { radio = it.animatedValue as Float }

        animator.addListener(object: Animator.AnimatorListener {

            override fun onAnimationEnd(animation: Animator?) {
                isTouch = false
                horarioListener(horarioTareasDias)
            }

            override fun onAnimationRepeat(animation: Animator?) {}
            override fun onAnimationCancel(animation: Animator?) {}
            override fun onAnimationStart(animation: Animator?) {}
        })
    }

    fun onClickHorarioListener(horarioListener: (HorarioTareasDias) -> Unit) {
        this.horarioListener = horarioListener
    }

    fun resaltarHorario() {
        paint.color = WHITE
        paint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD_ITALIC)
    }

    fun draw(c: Canvas) {

        c.drawText(titulo, (c.width / 2).toFloat(), y, paint)

        if (!isTouch) return

        c.save()
        c.clipRect(bound.left, bound.top, bound.right, bound.bottom)
        c.drawCircle(xTouch, yTouch, radio, paintRipple)
        c.restore()
    }

    fun isTouch(x: Float, y: Float, scrollY: Float): Boolean {

        xTouch = x
        yTouch = y - scrollY

        val limiteIzquierdo = bound.left
        val limiteDerecho = bound.right
        val limiteSuperior = scrollY + bound.top
        val limiteInferior = scrollY + bound.bottom

        isTouch = x > limiteIzquierdo && x < limiteDerecho &&
                y > limiteSuperior  && y < limiteInferior

        return isTouch
    }
}