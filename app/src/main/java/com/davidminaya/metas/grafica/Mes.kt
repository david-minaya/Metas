package com.davidminaya.metas.grafica

import android.graphics.Canvas
import android.graphics.Color.WHITE
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Typeface
import com.davidminaya.metas.utils.transparentar

/**
 * Creada por david minaya el 18/07/2018 12:17.
 */
class Mes(private var x: Float, private var texto: String) {

    private var xTexto: Float = 0f
    private val textoPaint: Paint

    /**
     * Retorna el cuadro delimitador del mes
     */
    private val bounds: RectF

    init {

        val isCurrentMonth = x == 0f
        this.x = START_X + x
        xTexto = x + ANCHO / 2
        bounds = RectF(this.x, 0f, this.x + ANCHO, 1f)

        textoPaint = Paint()
        textoPaint.isAntiAlias = true
        textoPaint.textSize = 14 * SP
        textoPaint.textAlign = Paint.Align.CENTER
        textoPaint.color = transparentar(WHITE, 90)

        if(isCurrentMonth) {
            textoPaint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        }
    }

    fun draw(c: Canvas) {

        if (!c.quickReject(bounds, Canvas.EdgeType.BW)) {

            // Linea
            c.drawLine(x, 0f, x, c.height - 20 * DP, lineaPaint)

            // Mes
            c.drawText(texto, xTexto, c.height - 16 * DP, textoPaint)
        }
    }

    companion object {

        private var DP: Float = 0.toFloat()
        private var SP: Float = 0.toFloat()
        var ANCHO: Float = 0f

        /*
         * Posicion X inicial de todos los meses. La posicion X de todos los meses es
         * relativa a esta posicion.
         * */
        private var START_X: Int = 0
        private lateinit var lineaPaint: Paint

        fun setDisplayMetrics(dp: Float, sp: Float, startX: Int, ancho: Int) {

            DP = dp
            SP = sp
            START_X = startX
            ANCHO = ancho.toFloat()

            lineaPaint = Paint()
            lineaPaint.isAntiAlias = true
            lineaPaint.strokeWidth = DP
            lineaPaint.color = transparentar(WHITE, 12)
        }
    }
}
