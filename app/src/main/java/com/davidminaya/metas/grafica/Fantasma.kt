package com.davidminaya.metas.grafica

import android.animation.AnimatorInflater
import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.Color.WHITE
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.provider.Settings
import android.text.Layout
import android.text.Spannable
import android.text.StaticLayout
import android.text.TextPaint
import com.davidminaya.metas.R
import com.davidminaya.metas.receivers.TareasReceiver
import com.davidminaya.metas.database.MetasDB
import com.davidminaya.metas.database.entities.Tarea
import com.davidminaya.metas.grafica.Fantasma.Tipo.*
import com.davidminaya.metas.utils.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.ChronoField

@Suppress("JoinDeclarationAndAssignment")
class Fantasma(private val context: Context, idFantasma: Int, spannable: Spannable, width: Int) {

    private val fantasma: Bitmap
    private var xFantasma = 0f
    private var yFantasma = 0f
    private val texto: StaticLayout
    private var xTexto = 0f
    private var yTexto = 0f
    private val animator: ValueAnimator
    private val dp: Float
    private val handler: Handler
    lateinit var actualizarFechaDeLaAplicacionListener: () -> Unit

    init {

        dp = context.resources.displayMetrics.density
        val sp = context.resources.displayMetrics.scaledDensity

        val paintTexto = TextPaint().apply {
            isAntiAlias = true
            textSize = 14 * sp
            color = WHITE
        }

        fantasma = BitmapFactory.decodeResource(context.resources, idFantasma)
        texto = StaticLayout(spannable, paintTexto, (280 * dp).toInt(), Layout.Alignment.ALIGN_CENTER, 1f, 0f, false)

        xFantasma = (width / 2f) - (fantasma.width / 2f)

        xTexto = (width / 2f) - ((280 * dp) / 2)
        yTexto= (152 * dp) + fantasma.height

        animator = AnimatorInflater.loadAnimator(context, R.animator.fantasma_animator) as ValueAnimator
        animator.addUpdateListener(this::onFantasmaAnimationUpdate)

        HandlerThread("Fantasma thread").apply {
            start()
            handler = Handler(looper)
        }
    }

    fun startAnimation() {
        animator.start()
    }

    private fun onFantasmaAnimationUpdate(valueAnimator: ValueAnimator) {
        yFantasma = valueAnimator.animatedValue as Float
    }

    fun draw(canvas: Canvas) {

        canvas.drawBitmap(fantasma, xFantasma, yFantasma, null)

        canvas.save()
        canvas.translate(xTexto, yTexto)
        texto.draw(canvas)
        canvas.restore()
    }

    fun onSingleTapUp(x: Float, y: Float) {

        enlace(97, 129, x, y, ENLACE_AJUSTAR)
        enlace(132, 167, x, y, ENLACE_ACTUALIZAR)
    }

    private fun enlace(inicioDelEnlace: Int, finalDelEnlace: Int, x: Float, y: Float, tipo: Tipo) {

        val enlace = Path()
        texto.getSelectionPath(inicioDelEnlace, finalDelEnlace, enlace)
        val boundsEnlace = RectF()
        enlace.computeBounds(boundsEnlace, true)
        boundsEnlace.offset(xTexto, yTexto)

        if (boundsEnlace.contains(x, y)) {

            when (tipo) {

                ENLACE_AJUSTAR -> {
                    context.startActivity(Intent(Settings.ACTION_DATE_SETTINGS))
                }

                ENLACE_ACTUALIZAR -> {
                    handler.post { actualizarTareas() }
                }
            }
        }
    }

    private fun actualizarTareas() {

        val db = MetasDB.getMetasDB(context)
        val tareas = db.tareaDao().tareas()

        val tareasActualizadas = mutableListOf<Tarea>()
        for (tarea in tareas) {

            if (!tarea.isFinalizada) {

                val dias = db.diasDao().dias(tarea.horarioId!!).sortedDescending()
                val diaActual = LocalDate.now().dayOfWeek.value
                val fechaDeInicio = LocalDate.parse(tarea.fechaDeInicio, DateTimeFormatter.ISO_LOCAL_DATE)

                var diaMenor = ES_MAYOR_AL_DIA_ACTUAL
                for (dia in dias) {

                    if (dia <= diaActual) {
                        diaMenor = dia.toLong()
                        break
                    }
                }

                var fechaActualizada = if (diaMenor != ES_MAYOR_AL_DIA_ACTUAL) {
                    LocalDate.now().with(ChronoField.DAY_OF_WEEK, diaMenor)
                } else {
                    LocalDate.now().minusWeeks(1).with(ChronoField.DAY_OF_WEEK, dias.max()!!.toLong())
                }

                if (fechaActualizada < fechaDeInicio) {
                    fechaActualizada = fechaDeInicio
                }

                tarea.fechaDeFinalizacion = fechaActualizada.format(DateTimeFormatter.ISO_LOCAL_DATE)
                tareasActualizadas.add(tarea)
            }
        }

        db.tareaDao().update(*tareasActualizadas.toTypedArray())

        Handler(Looper.getMainLooper()).post{
            context.sendBroadcast(Intent(context, TareasReceiver::class.java).apply { action = ACTION_CREAR_ALARMAS })
            actualizarFechaDeLaAplicacionListener()
        }

    }

    companion object {
        private const val ES_MAYOR_AL_DIA_ACTUAL = -1L
    }

    private enum class Tipo { ENLACE_AJUSTAR, ENLACE_ACTUALIZAR }
}