package com.davidminaya.metas.grafica

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.*
import android.graphics.Color.WHITE
import android.os.Handler
import android.os.Looper
import android.support.v4.view.GestureDetectorCompat
import android.text.*
import android.text.style.ForegroundColorSpan
import android.view.GestureDetector.SimpleOnGestureListener
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.EdgeEffect
import android.widget.OverScroller

import com.davidminaya.metas.R
import com.davidminaya.metas.database.entities.HorarioTareasDias
import com.davidminaya.metas.utils.transparentar

import java.util.ArrayList

import org.threeten.bp.LocalDate

/**
 * Creada por david minaya el 29/06/2018 11:03.
 */
class GraficaDeTareas : View {

    companion object {

        private var dp = 0f
        private var SP = 0f
        private var START_X = 0
        private var ANCHO = 0

        private lateinit var MESES: Array<String>
    }

    private var ejeHorizontalPaint = Paint()
    private var xMes = 0f
    private var nMes = 8
    private var meses = ArrayList<Mes>()

    private var unHorarioFueEliminado = false
    lateinit var onClickHorarioListener: (HorarioTareasDias) -> Unit

    /**
     * Administra el scroll de la grafica de tareas
     * @see Scroll
     */
    private var scroll = Scroll()
    private var scroller = OverScroller(context)
    private lateinit var gesture: GestureDetectorCompat

    private lateinit var edgeTop: EdgeEffect
    private lateinit var edgeLeft: EdgeEffect
    private lateinit var edgeRigth: EdgeEffect
    private lateinit var edgeBottom: EdgeEffect

    internal var edgeTopActive = false
    internal var edgeLeftActive = false
    internal var edgeRigthActive = false
    internal var edgeBottomActive = false

    private lateinit var fantasmaInfo: Fantasma
    private lateinit var fantasmaError: Fantasma

    var isFilter = false

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {

        dp = resources.displayMetrics.density
        SP = resources.displayMetrics.scaledDensity
        ANCHO = (150 * dp).toInt()
        MESES = resources.getStringArray(R.array.meses)
        xMes = -(ANCHO * 8).toFloat()

        Horario.setDisplayMetrics(dp, SP)
        Tarea.setDisplayMetrics(dp, SP, ANCHO)

        gesture = GestureDetectorCompat(context, SimpleGesture())
        setOnTouchListener { _, event ->  gesture.onTouchEvent(event)}

        edgeTop = EdgeEffect(context)
        edgeLeft = EdgeEffect(context)
        edgeRigth = EdgeEffect(context)
        edgeBottom = EdgeEffect(context)

        ejeHorizontalPaint.isAntiAlias = true
        ejeHorizontalPaint.color = transparentar(WHITE, 12)
        ejeHorizontalPaint.strokeWidth = 1 * dp
    }

    var horarios: List<Horario>? = null
        set(value) {

            if (field != null) {
                unHorarioFueEliminado = field!!.size > value!!.size
            }

            field = value
            calcularLimitesDelDesplazamiento()
        }

    var estado = Estado.NORMAL
        set(value) {
            field = value

            if (value == Estado.ERROR) {
                fantasmaError.startAnimation()
            }
        }

    fun actualizatFechaDeLaAplicacionListener(listener: () -> Unit) {
        fantasmaError.actualizarFechaDeLaAplicacionListener = listener
    }

    /*
    * Calcula el desplazamiento maximo de la grafica de tareas
    * */
    private fun calcularLimitesDelDesplazamiento() {

        if (horarios == null) return

        if (horarios!!.isEmpty()) {
            fantasmaInfo.startAnimation()
            invalidate()
        }

        val tareas = ArrayList<Tarea>()
        for (h in horarios!!) {
            tareas.addAll(h.tareas)
        }

        /* Espera a que la vista termine de realizar su diseño antes de establecer el
         * desplazamiento maximo de la grafica de tareas. Si el desplazamiento
         * maximo se establece antes de que la vista termine de realizar su diseño, el
         * valor minimo de desplazamiento vertical se establece mas bajo de lo necesario,
         * lo que causa que sobre espacio innecesario. Esto se debe a que para calcular
         * el valor del desplazamiento minimo vertical se utilza el valor retornado por
         * el metodo getHeight(), si este metodo se llama antes de que la vista termine
         * de calcular su diseño, el valor que este metodo retorna es cero, lo que
         * provoca un mal calculo del desplazamiento minimo de la grafica de tareas.
         */
        post {

            scroll.isHorizontalScrollEnable = false
            scroll.isVerticalScrollEnable = false

            if (tareas.size == 0) return@post

            val minLeft = tareas.minBy { it.bounds.left }!!.bounds.left
            val maxX = -(minLeft - 50 * dp).toInt()
            val minX = -(tareas.maxBy { it.bounds.right }!!.bounds.right.toInt() + (50 * dp)).toInt()
            val minY = -(horarios!!.last().bound.bottom - (height - 50 * dp)).toInt()

            scroll.isVerticalScrollEnable = horarios!!.last().bound.bottom > height
            scroll.isHorizontalScrollEnable = -minLeft > width || minX < 0
            val canScroll = scroll.y >= scroll.minY && scroll.y <= (scroll.minY + 84 * dp) && scroll.minY < 0

            scroll.setBoundsScroll(minX, maxX, minY)

            if (isFilter) {

                scroll.isVerticalScrollEnable = true
                scroll.y = 0
                invalidate()
                scroll.isVerticalScrollEnable = horarios!!.last().bound.bottom > height

                isFilter = false
                return@post
            }

            if (unHorarioFueEliminado && canScroll) {

                scroll.isVerticalScrollEnable = true

                Handler(Looper.getMainLooper()).post {

                    val toScrollY = if (horarios!!.last().bound.bottom > height) scroll.minY else 0

                    ObjectAnimator.ofInt(scroll.y, toScrollY).apply {
                        duration = 200
                        addUpdateListener {
                            scroll.y = it.animatedValue as Int
                            invalidate()
                        }
                        addListener(object: Animator.AnimatorListener {

                            override fun onAnimationEnd(animation: Animator?) {
                                scroll.isVerticalScrollEnable = horarios!!.last().bound.bottom > height
                            }

                            override fun onAnimationRepeat(animation: Animator?) {}
                            override fun onAnimationCancel(animation: Animator?) {}
                            override fun onAnimationStart(animation: Animator?) {}
                        })
                        start()
                    }
                }
            }

            invalidate()
        }
    }

    fun configurarTareaActual(tareaid: Int) {

        horarios!!.forEach { horario ->

            horario.tareas.forEach { tarea ->

                if (tareaid == tarea.tarea.tareaId) {

                    tarea.setTextColor()
                    horario.resaltarHorario()

                    // Calcula el desplazamiento de la gráfica para centrar el horario
                    // de la tarea actual en el centro de la gráfica de tareas.
                    var scrollY = height - (37 * dp)
                    scrollY /= 2
                    scrollY -= horario.bound.top + (32 * dp)

                    scroll.y = when {
                        scrollY > 0 -> 0
                        scrollY < scroll.minY -> scroll.minY
                        else -> scrollY.toInt()
                    }

                    invalidate()
                }
            }
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (horarios == null) return

        when (estado) {

            Estado.NORMAL -> {

                dibujarEjeHorizantal(canvas)
                dibujarMeses(canvas)
                dibujarHorarios(canvas)
                dibujarEdgeEffect(canvas)
            }

            Estado.VACIA -> {

                fantasmaInfo.draw(canvas)
                postInvalidateOnAnimation()
            }

            Estado.ERROR -> {

                fantasmaError.draw(canvas)
                postInvalidateOnAnimation()
            }
        }
    }

    private fun dibujarEjeHorizantal(c: Canvas) {

        val startMargin = 0f

        // Eje X
        val h = c.height - 36 * dp
        c.drawLine(startMargin, h, c.width.toFloat(), h, ejeHorizontalPaint)
    }

    private fun dibujarMeses(c: Canvas) {

        c.save()
        c.translate(scroll.x.toFloat(), 0f)

        for (mes in meses) {
            mes.draw(c)
        }

        c.restore()
    }

    private fun dibujarHorarios(c: Canvas) {

        if (horarios == null)
            return

        c.save()
        c.clipRect(1f, 0f, c.width.toFloat(), c.height - 37 * dp)
        c.translate(0f, scroll.y.toFloat())

        for (h in horarios!!) {

            if (!c.quickReject(h.bound, Canvas.EdgeType.AA)) {

                h.draw(c)
                dibujarTareas(c, h)

                if (h.rippleAnimationIsRunning) postInvalidateOnAnimation()
            }
        }

        c.restore()
    }

    private fun dibujarTareas(c: Canvas, h: Horario) {

        val anchoDiasMesActual = ANCHO / (LocalDate.now().lengthOfMonth() -1)
        val diasTranscurridos = LocalDate.now().dayOfMonth * anchoDiasMesActual

        /**
         * Calcula la posición cero de las tareas, que es la posición de la fecha actual
         *
         * |                    |                    |                    |
         * |                    |                    |                    |
         * |                    |             • posicion cero de las tareas
         * |                    |                    |                    |
         * |--------------------|--------------------|--------------------|
         * ^ scroll.x           ^ START_X     ^ días transcurridos
         *
         */
        val x = (scroll.x + START_X + diasTranscurridos).toFloat()

        c.save()
        c.translate(x, h.y)

        for (t in h.tareas) {

            t.calcularPosicionDelTexto(x)
            t.draw(c)
        }

        c.restore()
    }

    private fun dibujarEdgeEffect(c: Canvas) {

        if (!edgeTop.isFinished) {
            edgeTop.color = WHITE
            edgeTop.setSize(c.width, c.height)
            if (edgeTop.draw(c)) {
                postInvalidateOnAnimation()
            }
        }

        if (!edgeLeft.isFinished) {
            c.save()
            c.translate(0f, c.height.toFloat())
            c.rotate(-90f, 0f, 0f)
            edgeLeft.color = WHITE
            edgeLeft.setSize(c.height, c.width)
            if (edgeLeft.draw(c)) {
                postInvalidateOnAnimation()
            }
            c.restore()
        }

        if (!edgeRigth.isFinished) {
            c.save()
            c.translate(c.width.toFloat(), 0f)
            c.rotate(90f, 0f, 0f)
            edgeRigth.color = WHITE
            edgeRigth.setSize(c.height, c.width)
            if (edgeRigth.draw(c)) {
                postInvalidateOnAnimation()
            }
            c.restore()
        }

        if (!edgeBottom.isFinished) {
            c.save()
            c.translate(c.width.toFloat(), c.height - 37 * dp)
            c.rotate(180f, 0f, 0f)
            edgeBottom.color = WHITE
            edgeBottom.setSize(c.width, c.height)
            if (edgeBottom.draw(c)) {
                postInvalidateOnAnimation()
            }
            c.restore()
        }
    }

    inner class SimpleGesture : SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent): Boolean {

            scroll.reiniciarJuego()
            scroller.forceFinished(true)
            releaseEdge()

            return true
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {

            detectarHorarioTocado(e.x, e.y)
            fantasmaError.onSingleTapUp(e.x, e.y)

            return true
        }

        override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {

            val dx = (-distanceX).toInt()
            val dy = (-distanceY).toInt()

            scroll.setDistance(dx, dy)
            scroll.decidirDireccionDelScroll()
            scroll.updateScroll()

            addDimanicMes()

            if (scroll.isTopScrollBound) {
                edgeTop.onPull(1f)
                edgeTopActive = true
            }

            if (scroll.isLeftScrollBound) {
                edgeLeft.onPull(1f)
                edgeLeftActive = true
            }

            if (scroll.isRigthScrollBound) {
                edgeRigth.onPull(1f)
                edgeRigthActive = true
            }

            if (scroll.isBottomScrollBound) {
                edgeBottom.onPull(1f)
                edgeBottomActive = true
            }

            scroll.noScrollbound()

            postInvalidateOnAnimation()

            return true
        }
        
        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {

            releaseEdge()

            scroller.forceFinished(true)
            scroller.fling(scroll.x, scroll.y, velocityX.toInt(),
                    velocityY.toInt(), scroll.minX, scroll.maxX, scroll.minY, 0)

            postInvalidateOnAnimation()

            return true
        }
    }

    private fun releaseEdge() {

        edgeTop.onRelease()
        edgeTopActive = false
        edgeLeft.onRelease()
        edgeLeftActive = false
        edgeRigth.onRelease()
        edgeRigthActive = false
        edgeBottom.onRelease()
        edgeBottomActive = false
    }

    /**
     * Detecta la tarea tocada y ejecuta el evento OnTareaListener
     */
    private fun detectarHorarioTocado(x: Float, y: Float) {

        for (h in horarios!!) {

            if (h.isTouch(x, y, scroll.y.toFloat())) {
                invalidate()
                h.onClickHorarioListener {
                    onClickHorarioListener(it)
                }
            }
        }
    }

    override fun computeScroll() {
        super.computeScroll()

        if (scroller.computeScrollOffset()) {

            if (scroll.isHorizontalScroll) {

                if (!scroll.isHorizontalScrollEnable) return

                scroll.x = scroller.currX

                addDimanicMes()

                if (scroll.x >= scroll.maxX && !edgeLeftActive && edgeLeft.isFinished) {
                    edgeLeft.onAbsorb(scroller.currVelocity.toInt())
                    edgeLeftActive = true
                }

                if (scroll.x <= scroll.minX && !edgeRigthActive && edgeRigth.isFinished) {
                    edgeRigth.onAbsorb(scroller.currVelocity.toInt())
                    edgeRigthActive = true
                }

            } else {

                if (!scroll.isVerticalScrollEnable) return

                scroll.y = scroller.currY

                if (scroll.y >= 0 && !edgeTopActive && edgeTop.isFinished) {
                    edgeTop.onAbsorb(scroller.currVelocity.toInt())
                    edgeTopActive = true
                }

                if (scroll.y <= scroll.minY && !edgeBottomActive && edgeBottom.isFinished) {
                    edgeBottom.onAbsorb(scroller.currVelocity.toInt())
                    edgeBottomActive = true
                }

            }

            postInvalidateOnAnimation()
        }
    }

    /**
     * Agrega nuevos meses mientras el usuario desplaza la grafica
     * horizontalmente.
     */
    private fun addDimanicMes() {

        val xMes2 = xMes + Mes.ANCHO * 2

        if (-scroll.x < xMes2) {

            val n = LocalDate.now().minusMonths(nMes.toLong()).monthValue

            meses.add(Mes(xMes, MESES[n -1]))
            xMes += -Mes.ANCHO

            nMes += 1
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val modeW = View.MeasureSpec.getMode(widthMeasureSpec)
        val sizeW = View.MeasureSpec.getSize(widthMeasureSpec)
        val modeH = View.MeasureSpec.getMode(heightMeasureSpec)
        val sizeH = View.MeasureSpec.getSize(heightMeasureSpec)

        val minW = paddingStart + paddingEnd + (150 * dp).toInt()
        val minH = paddingTop + paddingBottom + (150 * dp).toInt()

        var w = minW
        var h = minH

        // Width
        when (modeW) {
            View.MeasureSpec.AT_MOST -> w = Math.min(sizeW, minW)
            View.MeasureSpec.EXACTLY -> w = sizeW
            View.MeasureSpec.UNSPECIFIED -> w = minW
        }

        // Height
        when (modeH) {
            View.MeasureSpec.AT_MOST -> h = Math.min(sizeH, minH)
            View.MeasureSpec.EXACTLY -> h = sizeH
            View.MeasureSpec.UNSPECIFIED -> h = minH
        }

        setMeasuredDimension(w, h)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        Horario.widthView = w
        Tarea.widthDisplay = w

        // calcula la posición cero de la grafica de tareas. En esta
        // posición se dibuja el mes actual. Y a partir de esta
        // posición se dibujan todas las tareas.
        START_X = (w - ANCHO)

        calcularLimitesDelDesplazamiento()
        crearMesesIniciales()
        crearFantasmas()
    }

    /**
     * Crea los primeros meses que se dibujan en la grafica de tareas
     * */
    private fun crearMesesIniciales() {

        Mes.setDisplayMetrics(dp, SP, START_X, ANCHO)
        meses.clear()

        var i = 7 // establece la posicion en que se dibujaran los meses
        var x = -(ANCHO * 7).toFloat()
        while (i > -7) {

            var n = 1
            if (!isInEditMode) {
                // Obtiene el nombre del mes
                n = LocalDate.now().minusMonths(i.toLong()).monthValue
            }

            meses.add(Mes(x, MESES[n - 1]))
            i--
            x += Mes.ANCHO.toInt()
        }
    }

    private fun crearFantasmas() {

        val spanInfo = SpannableString(context.getString(R.string.mensaje_fantasma_info))

        val spanError = SpannableString(context.getString(R.string.mensaje_fantasma_error)).apply {
            setSpan(ForegroundColorSpan(Color.parseColor("#008e76")), 97, 129, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            setSpan(ForegroundColorSpan(Color.parseColor("#008e76")), 132, 167, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        fantasmaInfo = Fantasma(context, R.drawable.fantasma, spanInfo, width)
        fantasmaError = Fantasma(context, R.drawable.fantasma_error, spanError, width)
    }
}

enum class Estado { VACIA, ERROR, NORMAL }
