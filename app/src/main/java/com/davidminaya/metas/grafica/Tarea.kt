package com.davidminaya.metas.grafica

import android.graphics.*
import android.graphics.Color.WHITE
import com.davidminaya.metas.utils.*

import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.ChronoUnit
import org.threeten.bp.temporal.TemporalAdjusters

/**
 * Creada por david minaya el 19/07/2018 11:42.
 */
class Tarea(val tarea: com.davidminaya.metas.database.entities.Tarea) {

    private var x = 0f

    private var xTitulo = 0f
    private var y = 0f
    private var width = 0f
    private val color: Int

    private val texto: String
    private val paintTexto = Paint()
    private var widthText: Float = 0.toFloat()

    private val barra: RectF

    /**
     * Cuadro delimitador de la tarea
     *
     * ```
     * +-----------------------------+
     * |          Tarea              |
     * |  (///////////////////////)  |
     * +-----------------------------+
     * ```
     */
    lateinit var bounds: RectF

    private val paintBarra: Paint

    companion object {

        private var SP = 0f
        private var dp = 0f
        var widthDisplay = 0

        /*
         * Ancho de las tareas
         * */
        private var ancho = 0f

        fun setDisplayMetrics(dp: Float, sp: Float, startX: Int) {

            this.dp = dp
            SP = sp
            ancho = startX.toFloat()
        }
    }

    init {

        val fechaDeInicio = LocalDate.parse(tarea.fechaDeInicio, DateTimeFormatter.ISO_LOCAL_DATE)
        val fechaDeFinalizacion = LocalDate.parse(tarea.fechaDeFinalizacion, DateTimeFormatter.ISO_LOCAL_DATE)

        x = calcularPosicionDeLaFecha(fechaDeInicio)
        y = 28 * dp
        width = calcularPosicionDeLaFecha(fechaDeFinalizacion)
        color = tarea.color
        texto = tarea.nombre

        paintTexto.isAntiAlias = true
        paintTexto.color = transparentar(WHITE, 100)
        paintTexto.textSize = 13 * SP

        widthText = paintTexto.measureText(texto)

        // float left, float top, float right, float bottom
        barra = RectF(x, y + (8 * dp), width, y + (20 * dp))

        paintBarra = Paint()
        paintBarra.isAntiAlias = true
        paintBarra.color = color

        calcularPosicionDelTexto(0f)
        calcularBounds()
    }

    /**
     * Calcula la posicion de la fecha en la grafica de tareas
     *
     * Este metodo es utilizado para calcular la posicion de la fecha de inicio o de la fecha de
     * finalizacion de la tarea. La fecha de inicio es utilizada para calcular la posicion de
     * inicio de la tarea y la fecha de finalizacion es utilizada para calcular la posicion de
     * finalizacion de la tarea. Por ejemplo, si este metodo recibe como parametro la fecha de
     * inicio de la tarea, retorna como resultado la posicion de inicio de la tarea.
     *
     * La posicion de la fecha en la grafica de tareas se calcula en base a los dias transcurridos
     * desde la fecha hasta la fecha actual. La posicion de la fecha actual en la grafica de tareas
     * es de cero. Ejemplo, si desde una fecha hasta la fecha actual han transcurrido 100 dias,
     * esta fecha se colocara a una distancia de -100 de la fecha actual.
     *
     * ```
     * Fecha de inicio: 4 de Enero
     * Fecha de finalizacion: 20 de Febrero
     * Fecha actual: 6 de Abril
     *
     * |                    |                    |                    |                    |
     * |                    |                    |                    |                    |
     * |  (///////////////////////////////)      |                    |   •                |
     * |  ^ Posicion de inicio            ^ Posicion de finalizacion  |   ^ Fecha actual   |
     * |                    |                    |                    |                    |
     * |--------------------|--------------------|--------------------|--------------------|
     * |       Enero        |       Febrero      |        Marzo       |   Abril (actual)   |
     * ```
     *
     * Dependiendo de la cantidad de tiempo transcurrido desde la fecha hasta la fecha actual la
     * posicion de la fecha se calcula de diferentes maneras:
     *
     * 1. Si la fecha es igual a la fecha actual se omite el calculo de la fecha y se retorna cero.
     *
     * 2. Si el mes de la fecha es igual al mes de la fecha actual, se calcula la cantidad de dias
     * transcurridos entre las dos fechas y se multiplican por el ancho de los dias del mes actual.
     *
     * 3. Si el mes de la fecha es anterior al mes actual se realizan varios calculos. Primero se
     * obtienen los dias restantes del mes de la fecha y se multiplican por el ancho de los dias de
     * ese mes. Despues se obtiene la cantidad de meses que han transcurrido desde la fecha hasta
     * la fecha actual y se multiplican por el ancho de los meses. Y por ultimo se obtienen los
     * dias que han transcurrido en el mes actual y se multiplican por el ancho de los dias de el
     * mes actual.
     *
     * ```
     * |                    |                    |                    |                    |
     * |          Dias      |                    |                    |     Dias           |
     * |        restantes   |           meses transcurridos           | transcurridos      |
     * |     *========================================================================>    |
     * |     ^ Fecha        |                    |                    |  Fecha actual ^    |
     * |                    |                    |                    |                    |
     * |--------------------|--------------------|--------------------|--------------------|
     * |     Mes fecha      |           Meses intermendios            |  Mes fecha actual  |
     * ```
     *
     * Recuerde que este metodo calcula la distancia de una fecha a la vez, esta fecha puede ser la
     * fecha de inicio o la fecha de finalizacion de la tarea, no ambas. Por lo que en el ejemplo
     * anterior la fecha calculada puede ser cualquiera de la dos.
     *
     * Entre las fechas que tienen meses intermedios se calculan los meses en lugar de los dias
     * porque se obtiene el mismo resultado, con la ventaja de que calcular los meses requiere
     * menos pasos.
     *
     * Esto se debe a que el ancho de todos los meses es el mismo, lo que varia es el ancho de los
     * dias. Por ejemplo, si el ancho de un meses mes es de 150 y tiene 30 dias, el ancho de sus
     * dias sera de 5.1. Mientras que si el mes tiene 31 dias el ancho de sus dias sera de 5.
     *
     * Para saber el ancho de los dias de un mes, este se divide entre la cantidad de dias que
     * tiene menos 1. Ej: 150 / (mes.dias - 1). Esto se realiza de esta manera ya que un mes de 28
     * dias solo tiene una duracion de 27 dias.
     *
     * Ej:
     *
     * ```
     *  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28
     *  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
     *  |--------------------------------------------------------------------------------------------------------------------------------------|
     *  |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |  9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 |
     *  +--------------------------------------------------------------------------------------------------------------------------------------+
     * ```
     *
     * Como se puede ver en el ejemplo enterior los numeros que estan afuera representan los dias
     * del mes y los numeros que estan adentro representan la duracion de los dias del mes.
     *
     * Por ultimo la posicion maxima que puede tener una fecha en la grafica de tareas es de cero.
     * Esto se debe a que no se puede mostrar que una tarea se esta realizando en el futuro, por lo
     * que la fecha maxima que puede tener una tarea es igual a la fecha actual. Si este metodo
     * recibe como parametro una fecha mayor a la fecha actual lanza una exepcion.
     *
     * @param fecha Fecha de inicio o de finalizacion de la tarea
     *
     * @return Posicion de la fecha
     *
     * @exception Exception Si la fecha es mayor a la fecha actual
     * */
    private fun calcularPosicionDeLaFecha(fecha: LocalDate): Float {

        val fechaActual = LocalDate.now()
        val anchoDiasMesInicial = ancho / (fecha.lengthOfMonth() - 1)
        val anchoDiasMesActual = ancho / (fechaActual.lengthOfMonth() - 1)

        if (fecha > fechaActual)
            throw Exception("La fecha no puede ser mayor a la fecha actual. fecha: $fecha, fecha actual: $fechaActual")

        // Si ambas fechas son iguales
        if (fecha.isEqual(fechaActual)) {
            return 0f

        // Si ambas fechas tienen el mismo mes y el mismo año
        } else if (fecha.year == fechaActual.year &&
                fecha.month == fechaActual.month &&
                fecha.dayOfMonth < fechaActual.dayOfMonth) {

            var diasEntreFechas = (fecha.dayOfMonth - fechaActual.dayOfMonth).toFloat()
            diasEntreFechas *= anchoDiasMesActual

            return diasEntreFechas

        // Si la fecha es anterior a la fecha actual
        } else {

            var diasRestantes = (fecha.lengthOfMonth() - fecha.dayOfMonth).toFloat()
            var mesesIntermedios = ChronoUnit.MONTHS.between(fecha.with(TemporalAdjusters.lastDayOfMonth()), fechaActual).toFloat()
            var diasTranscurridos = (fechaActual.dayOfMonth).toFloat()

            diasRestantes *= anchoDiasMesInicial
            mesesIntermedios *= ancho
            diasTranscurridos *= anchoDiasMesActual

            val distanciaTotal = diasRestantes + mesesIntermedios + diasTranscurridos

            return -distanciaTotal
        }
    }

    /**
     * Si la barra de progreso es mas ancha que el texto, el texto se centra y se
     * coloca en el centro de la barra de progreso. De lo contrario el texto no se
     * alinea y se coloca en el borde izquierdo de la barra de progreso.
     */
    fun calcularPosicionDelTexto(dx: Float) {

        if (barra.width() > widthText) {

            paintTexto.textAlign = Paint.Align.CENTER

            val xt = (barra.left + (widthDisplay / 2)) - ((barra.right + dx) - barra.width())
            val margen = 40 * dp
            val mitadTexto = paintTexto.measureText(texto) / 2

            // 10------*--------------------*------100
            //                   texto
            if (xt > (barra.left + margen + mitadTexto) && xt < (barra.right - margen - mitadTexto)) {

                xTitulo = xt

            } else {

                // 10------*--------------------*------100
                //                               texto
                xTitulo = if (xt > (barra.right - margen - mitadTexto)) {
                    barra.right - margen - mitadTexto
                }

                // 10------*--------------------*------100
                //   texto
                else {
                    barra.left + margen + mitadTexto
                }
            }

        } else {

            xTitulo = x
        }
    }

    /**
     * Establece los limites de la tarea
     */
    private fun calcularBounds() {

        // Obtiene la altura del texto
        val rectText = Rect()
        paintTexto.getTextBounds(texto, 0, texto.length, rectText)
        val heightTexto = rectText.height()

        // Establece el limite superior e inferior de la tarea
        bounds = RectF()
        bounds.top = this.y - heightTexto
        bounds.left = barra.left
        bounds.bottom = barra.bottom

        // Si la barra de porgreso es mas ancha que el texto, el ancho de la
        // tarea es del ancho de la barra de porgreso, de lo contrario es del
        // ancho del texto. El limite derecho de la tarea se establece de acuerdo
        // a este ancho.
        if (barra.width() > widthText) {
            bounds.right = barra.right

        } else {
            bounds.right = widthText + x
        }
    }

    fun setTextColor() {
        paintTexto.color = WHITE
        paintTexto.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
    }

    /**
     * Dibuja la tarea
     *
     * @param c objeto canvas
     * */
    fun draw(c: Canvas) {

        // Si la tarea esta dentro de la pantalla
        if (!c.quickReject(bounds, Canvas.EdgeType.AA)) {

            c.drawText(texto, xTitulo, y, paintTexto)
            c.drawRoundRect(barra, barra.height() / 2f, barra.height() / 2f, paintBarra)

            if (!tarea.isFinalizada) {
                c.drawCircle(width + (8 * dp), y + (14 * dp), (2 * dp), paintBarra)
            }
        }
    }
}
