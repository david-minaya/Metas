package com.davidminaya.metas.grafica

import android.util.Log

/**
 * Creada por David Minaya el 19/07/2018 a las 1:00 PM
 *
 * Esta clase se encarga de administrar el scroll de la grafica de tareas.
 *
 * Esta clase cumple con las siguientes funciones:
 *
 * 1. Decidir la direccion de desplazamiento de la grafica de tareas.
 * 2. Establecer los limites de desplazamiento de la grafica de tareas.
 * 3. Decidir la posicion de desplazamiento actual de la grafica de tareas.
 * 4. Saber cuando la grafica se ha desplazado hasta alguno de sus limites.
 */
class Scroll {

    var x: Int = 0
    var y: Int = 0
    private var dx: Int = 0
    private var dy: Int = 0
    var isVerticalScrollEnable = false
    var isHorizontalScrollEnable = false
    var minX: Int = 0
        private set
    var maxX: Int = 0
        private set
    var minY: Int = 0
        private set

    /**
     * Variables utilizadas para decidir la direccion de desplazamiento de la
     * grafica de tareas. See [Scroll.decidirDireccionDelScroll].
     */
    private var jugadorX: Int = 0
    private var jugadorY: Int = 0

    /**
     * Mientras esta variable sea verdadera se tiene que volver a decidir la
     * direccion de desplazamiento de la grafica de tareas. Mire
     * [Scroll.decidirDireccionDelScroll]
     */
    private var partida = true

    /**
     * Si esta variable es verdadera, la direccion de desplazamiento de la grafica
     * de tareas es horizontal. Si es falsa, la direccion de desplazamiento es
     * vertical. See [Scroll.decidirDireccionDelScroll].
     */
    var isHorizontalScroll: Boolean = false
        private set

    /**
     * Estas variables son verdaderas cuando la grafica de tareas llega a alguno de
     * sus limites de desplazamiento.
     */
    var isTopScrollBound: Boolean = false
        private set
    var isLeftScrollBound: Boolean = false
        private set
    var isRigthScrollBound: Boolean = false
        private set
    var isBottomScrollBound: Boolean = false
        private set

    fun setDistance(dx: Int, dy: Int) {
        this.dx = dx
        this.dy = dy
    }

    fun setBoundsScroll(minX: Int, maxX: Int, minY: Int) {
        this.minX = minX
        this.maxX = maxX
        this.minY = minY
    }

    /**
     * Establece las variables de los limites de desplzamiento en falso.
     */
    fun noScrollbound() {
        isTopScrollBound = false
        isLeftScrollBound = false
        isRigthScrollBound = false
        isBottomScrollBound = false
    }

    /**
     * Este metodo decide la direccion de desplazamiento de la grafica de tareas.
     *
     * Para decidir la direccion de desplazamiento de la grafica se realiza
     * un juego. El juego consiste en medir la distancia de desplazamiento de
     * los ejes X y Y. El eje que más se mueva gana, y el scroll de la gráfica
     * se realiza en esa dirección.
     *
     * El funcionamiento del juego es el siguiente:
     *
     * 1. Un nuevo juego se inicia cada vez que el usuario toca la pantalla.
     *
     * 2. Cuando el usuario mueve el dedo por la pantalla se comienzan a jugar
     * las partidas. Las partidas se juegan hasta que una partida sea exitosa
     * o hasta que el usuario deje de tocar la pantalla.
     *
     * 3. Antes de comparar la distancia recorrida por los ejes, el valor de estos
     * se evalua y si estos poseen valores negativos, estos son convertidos a
     * positivo. Este paso se realiza para hacer mas facil la comparancion de
     * las distancias recorridas por los ejes.
     *
     * 4. Si el desplazamiento de los ejes X y Y es igual a cero, la partida no
     * es valida y esta es interrumpida. Como la partida no es valida se
     * pueden seguir jugando mas partidas hasta que una sea exitosa.
     *
     * 5. Una partida es exitosa cuando se decide cual de los dos ejes es el que
     * mas se ha movido.
     *
     * 6. Cuando una partida es exitosa, se dejan de jugar partidas hasta el
     * proximo scroll.
     *
     * 7. Cuando el usuario vuelve a tocar la pantalla, el juego finaliza y se
     * reinician sus valores. Esto deja el juego listo hasta que el usuario
     * vuelva a realizar scroll. El juego se reinicia con el metodo:
     * [Scroll.reiniciarJuego]
     *
     */
    fun decidirDireccionDelScroll() {

        // Si ya se ha jugado una partida exitosa se interrumpe la ejecucion del metodo
        if (!partida) return

        // Si x o y poseen numeros negativos, estos son convertidos a positivos y se
        // asignan a los jugadores.
        jugadorX = if (dx < 0) -dx else dx
        jugadorY = if (dy < 0) -dy else dy

        // Si el valor de ambos jugadores es igual a cero, la partida no es valida y se
        // interrumpe la ejecucion del metodo para jugar una nueva partida.
        if (jugadorX == 0 && jugadorY == 0) return

        // Si el valor del jugadorX es mayor que el del jugadorY, el jugadorX es el
        // ganador, de lo contrario el ganador es el jugadorY. Si el jugadorX es el
        // ganador a la variable isHorizontalScroll se le asigna el valor true, de lo contrario
        // se le asigna el valor false.
        isHorizontalScroll = jugadorX > jugadorY

        // Si se ha podido decidir un ganador, entonces la partida es exitosa y
        // a la variable partida se le asigna el valor false para que no se puedan
        // jugar mas partidas hasta que se reinicie el scroll.
        partida = false
    }

    /**
     * Actualiza los valores de desplazamiento de la grafica de tareas.
     */
    fun updateScroll() {

        if (isHorizontalScroll) {

            if (!isHorizontalScrollEnable) return

            // Esta condicion es verdadera cuando el desplazamiento horizontal de la
            // grafica ha llegado a alguno de sus limites. Si es asi, el valor de
            // desplzamiento de la grafica se establece en su valor minimo o maximo.
            // De lo contrario el desplazamiento de la grafica se establece en el valor
            // actual de desplazamiento.
            if (x + dx > maxX || x + dx < minX) {

                // Limite izquierdo. El desplazamiento horizontal de la grafica se establece en
                // su valor maximo.
                if (x + dx > maxX) {

                    x = maxX
                    isLeftScrollBound = true

                    // Limite derecho. El desplazamiento horizontal de la grafica se establece en
                    // su valor minimo.
                } else {

                    x = minX
                    isRigthScrollBound = true
                }

                // El desplazamiento horizontal de la grafica se establece en su valor de desplazamiento
                // actual.
            } else {

                x += dx
            }

        } else {

            if (!isVerticalScrollEnable) return

            // Esta condicion es verdadera cuando el desplazamiento vertical de la
            // grafica ha llegado a alguno de sus limites. Si es asi, el valor de
            // desplzamiento de la grafica se establece en su valor minimo o maximo.
            // De lo contrario el desplazamiento de la grafica se establece en el valor
            // actual de desplazamiento.
            if ((y + dy) < minY || (y + dy) > 0) {

                // Limite inferior. El desplazamiento vertical de la grafica se establece en
                // su valor minimo.
                if (y + dy < minY) {

                    Log.i("Scroll", "y + dy = ${y+dy}, y = $y, menor")

                    y = minY
                    isBottomScrollBound = true

                    // Limite superior. El desplazamiento vertical de la grafica se establece en
                    // su valor maximo.
                } else {

                    Log.i("Scroll", "y + dy = ${y+dy}, y = $y, mayor")

                    y = 0
                    isTopScrollBound = true
                }
            }

            // El desplazamiento vertical de la grafica se establece en su valor de desplazamiento
            // actual.
            else {

                y += dy
            }
        }
    }

    internal fun reiniciarJuego() {

        jugadorX = 0
        jugadorY = 0
        partida = true
    }
}
