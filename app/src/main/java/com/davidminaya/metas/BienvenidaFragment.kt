package com.davidminaya.metas

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_bienvenida.*

class BienvenidaFragment : Fragment() {

    private lateinit var imagen: Bitmap
    private lateinit var descripcion: String
    private var esLaPrimeraPantalla = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        imagen = BitmapFactory.decodeResource(resources, arguments!!.getInt("idImagen"))
        descripcion = arguments!!.getString("descripcion")
        esLaPrimeraPantalla = arguments!!.getBoolean("es_la_primera_pantalla")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(activity).inflate(R.layout.fragment_bienvenida, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!esLaPrimeraPantalla) {
            bienvenido.visibility = View.GONE
            fantasmaImageView.visibility = View.GONE
        }

        imagenImageView.setImageBitmap(imagen)
        textViewDescripcion.text = descripcion
    }

    companion object {

        fun createNewInstance(idImagen: Int, descripcion: String, esLaPrimeraPantalla: Boolean = false): Fragment {

            val bundle = Bundle().apply {
                putInt("idImagen", idImagen)
                putString("descripcion", descripcion)
                putBoolean("es_la_primera_pantalla", esLaPrimeraPantalla)
            }

            return BienvenidaFragment().apply {
                arguments = bundle
            }
        }

    }
}
