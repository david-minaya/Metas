package com.davidminaya.metas.receivers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import com.davidminaya.metas.database.MetasDB
import com.davidminaya.metas.database.entities.Tarea
import com.davidminaya.metas.servicios.EjecutarTareaService
import com.davidminaya.metas.utils.*
import com.davidminaya.metas.utils.ParcelableUtil
import org.threeten.bp.*
import org.threeten.bp.temporal.ChronoField

class TareasReceiver : BroadcastReceiver() {

    private lateinit var context: Context

    override fun onReceive(context: Context, intent: Intent) {

        this.context = context

        val thread = HandlerThread("Tareas receiver thread")
        thread.start()
        val handler = Handler(thread.looper)

        when (intent.action) {

            ACTION_EJECUTAR_TAREA -> {

                val bytes = intent.getByteArrayExtra(EXTRA_TAREA)
                val tarea = ParcelableUtil.unmarshall(bytes, Tarea.CREATOR)

                val intentStartService = Intent(context, EjecutarTareaService::class.java)
                intentStartService.putExtra(EXTRA_TAREA, tarea)
                context.startService(intentStartService)
            }

            ACTION_CREAR_ALARMAS, Intent.ACTION_BOOT_COMPLETED -> {
                handler.post(VolverACrearAlarmas(context))
            }
        }
    }

    private class VolverACrearAlarmas(val context: Context): Runnable {

        override fun run() {

            val db = MetasDB.getMetasDB(context)
            val horariosTareaDias = db.horarioDao().horariosTareaDias()

            for (horarioTareaDias in horariosTareaDias) {

                val tarea = horarioTareaDias.tarea
                val dias = horarioTareaDias.dias!!
                val diaActual = LocalDate.now().dayOfWeek.value
                val horaActual = LocalTime.now()
                val horaDeInicio = LocalTime.parse(horarioTareaDias.horario?.horaDeInicio)

                if (tarea!!.isFinalizada) continue

                for (dia in dias) {

                    val nDia = dia.nombreDiaId!!.toLong()
                    var fechaDeEjecucion: Long

                    if ((nDia >= diaActual && horaDeInicio > horaActual) || nDia > diaActual) {

                        fechaDeEjecucion = LocalDateTime.now()
                                .with(ChronoField.DAY_OF_WEEK, nDia)
                                .withHour(horaDeInicio.hour)
                                .withMinute(horaDeInicio.minute)
                                .withSecond(0)
                                .toInstant(OffsetDateTime.now().offset) // Obtiene la zona horaria actual
                                .toEpochMilli()
                    } else {

                        fechaDeEjecucion = LocalDateTime.now()
                                .plusWeeks(1)
                                .with(ChronoField.DAY_OF_WEEK, nDia)
                                .withHour(horaDeInicio.hour)
                                .withMinute(horaDeInicio.minute)
                                .withSecond(0)
                                .toInstant(OffsetDateTime.now().offset) // Obtiene la zona horaria actual
                                .toEpochMilli()
                    }

                    val intent = Intent(context, TareasReceiver::class.java).apply {
                        action = ACTION_EJECUTAR_TAREA
                        putExtra(EXTRA_TAREA, ParcelableUtil.marshall(tarea))
                    }

                    val pendingId = ((tarea.tareaId!! * 7) + nDia).toInt()
                    val pendingIntent = PendingIntent.getBroadcast(context, pendingId, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, fechaDeEjecucion, pendingIntent)
                    } else {
                        alarmManager.setExact(AlarmManager.RTC, fechaDeEjecucion, pendingIntent)
                    }
                }
            }
        }
    }
}
